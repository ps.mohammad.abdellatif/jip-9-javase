package com.progressoft.jip9.utils;

public class NumberBox<N extends Number> extends Box<N> {

}

class InheritanceTest {
    public static void main(String[] args) {
        NumberBox<Integer> numBox = new NumberBox<>();
        numBox.put(1);
        Integer item = numBox.item();

        Box<? super Integer> box = numBox;
    }
}

