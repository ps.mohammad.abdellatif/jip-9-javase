package com.progressoft.jip9.utils;

// generics: compile-time feature
public class Box<ITEM> {
    // General purpose box
    private ITEM item;

    public Box() {
    }

    public boolean isEmpty() {
        return item == null;
    }

    public Box(ITEM value) {
        this.item = value;
    }

    public ITEM item() {
        return item;
    }

    // in parameters
    public ITEM put(ITEM value) {
        ITEM old = this.item;
        this.item = value;
        return old;
    }
}
