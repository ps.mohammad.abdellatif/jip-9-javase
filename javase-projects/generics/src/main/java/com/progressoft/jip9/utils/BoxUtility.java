package com.progressoft.jip9.utils;

public class BoxUtility {

    public static <T> void moveItem(Box<? extends T> from, Box<? super T> to) {
        T item = from.put(null);
        to.put(item);
    }

    public static <T> void fillBox(Box<? super T> box, ItemSupplier<? extends T> supplier) {
        T item = supplier.supply();
        box.put(item);
    }

    public static <T> T newInstance(Class<T> type) {
        try {
            return type.newInstance();
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
    }
}
