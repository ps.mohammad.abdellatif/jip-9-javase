package com.progressoft.jip9.utils;

import java.util.*;

public class PointsMain {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int count = scanner.nextInt();
        Set<Point> points = new LinkedHashSet<>();

        while (count > 0) {
            int x = scanner.nextInt();
            int y = scanner.nextInt();
            Point p = new Point(x, y);
            boolean accepted = points.add(p);
            if (!accepted) {
                System.out.println("duplicate");
                continue;
            }
            count--;
        }

        for (Point p : points) {
            System.out.println(p);
        }

//        Iterator<Point> iterator = points.iterator();
//
//        while (iterator.hasNext()) {
//            Point point = iterator.next();
//            System.out.println(point);
//        }
    }
}
