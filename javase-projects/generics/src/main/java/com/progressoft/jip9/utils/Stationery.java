package com.progressoft.jip9.utils;

public class Stationery {

    public String name() {
        return this.getClass().getName();
    }
}

class Pen extends Stationery {

}

class Pencil extends Stationery {

}

class InkPen extends Pen {

}