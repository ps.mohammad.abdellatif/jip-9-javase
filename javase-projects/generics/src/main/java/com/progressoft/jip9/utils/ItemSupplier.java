package com.progressoft.jip9.utils;

public interface ItemSupplier<T> {

    T supply();
}


class PenSupplier implements ItemSupplier<Pen> {

    @Override
    public Pen supply() {
        return new Pen();
    }
}