package com.progressoft.jip9.utils;

public class BoxUseMain {
    // to pass in parameters (use super)
    // to read out parameters (use extends)

    public static void main(String[] args) {
        Pen pen = new Pen();
        Pencil pencil = new Pencil();

        Box<Pen> samiBox = new Box<>();
        Box<Pencil> ahmadBox = new Box<>();

        samiBox.put(pen);
        ahmadBox.put(pencil);

        checkBox(samiBox);
        checkBox(ahmadBox);

        Box<Stationery> stationeryBox = new Box<>();
        Box<Object> objBox = new Box<>();
        putPenInBox(samiBox);
        putPenInBox(stationeryBox);
        putPenInBox(objBox);

        deliverBoxToSami(samiBox);
        deliverBoxToAhmad(ahmadBox);
    }

    public static void movePen(Box<? extends Pen> from, Box<? super Pen> to) {
        Pen item = from.put(null);
        to.put(item);
    }

    //
    public static void putPenInBox(Box<? super Pen> box) {
        Pen pen = new Pen();
        box.put(pen);
        Object obj = box.item();
    }

    // I only need to check the box
    public static void checkBox(Box<? extends Stationery> box) {
        // out will always be the upper bound (Stationery)
        Stationery stationery = box.item();
        // passing for in parameters is forbidden
        //box.put(new Pen());
        //box.put(new Pencil());
        // this is allowed
        //box.put(null);

        System.out.println("check if box has item or not");
        if (box.isEmpty())
            throw new IllegalStateException("the box is empty :(");
    }

    public static void deliverBoxToSami(Box<Pen> box) {
        System.out.println("i am expected a pen");
        Pen pen = box.item();
        System.out.println("I got my pen :)");
    }

    public static void deliverBoxToAhmad(Box<Pencil> box) {
        System.out.println("i am expecting a pencil");
        Pencil pencil = box.item();
        System.out.println("I got my pencil :)");
    }
}
