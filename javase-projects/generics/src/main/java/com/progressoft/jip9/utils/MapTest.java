package com.progressoft.jip9.utils;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public class MapTest {
    // for a utility accepts a string and returns the characters repeat count
    // Hello world from Mohammad
    // H:1
    // e:1
    //l:3
    // etc....

    public static void main(String[] args) {
        Map<Integer, String> map = new LinkedHashMap<>();


        map.put(3, "THREE");
        map.put(1, "ONE");// add or replace
        map.put(2, "TWO");
        map.put(0, "ONE");

        map.put(1, "one");
        map.put(null, "NULL");

        System.out.println(map.containsKey(1));
        System.out.println(map.get(3));

        Set<Integer> keys = map.keySet();
        for (Integer key : keys) {
            System.out.println(map.get(key));
        }

        Set<Map.Entry<Integer, String>> entries = map.entrySet();
        for (Map.Entry<Integer, String> entry : entries) {
            System.out.println(entry.getKey() + "=" + entry.getValue());
        }
    }
}
