package com.progressoft.jip9.utils;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class BoxTest {
    // Box to hold one single value of type string

    @Test
    public void givenValue_whenConstructBox_thenBoxIsConstructedHoldingTheValue() {
        Box box = new Box("Hello");
        Assertions.assertEquals("Hello", box.item());

        Object old = box.put("Welcome");
        Assertions.assertEquals("Hello", old);
        Assertions.assertEquals("Welcome", box.item());
    }
}
