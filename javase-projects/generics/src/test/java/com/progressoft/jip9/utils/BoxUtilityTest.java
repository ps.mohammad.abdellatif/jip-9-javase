package com.progressoft.jip9.utils;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class BoxUtilityTest {

    @Test
    public void givenTwoBoxes_whenMoveItemBetweenThem_thenItemIsMoved() {
        Box<InkPen> from = new Box<>();
        Box<Object> to = new Box<>();

        InkPen item = new InkPen();
        from.put(item);

        // type inferring
        BoxUtility.moveItem(from, to);
        Assertions.assertTrue(from.isEmpty(), "from box should be empty");
        Assertions.assertFalse(to.isEmpty(), "to box should not be empty");
        Assertions.assertSame(item, to.item(), "item was not moved");

        Box<String> strFrom = new Box<>();
        Box<Object> objTo = new Box<>();

        strFrom.put("hello world");
        BoxUtility.<String>moveItem(strFrom, objTo);
    }

    @Test
    public void givenBoxAndSupplier_whenFillBox_thenBoxIsFilled() {
        Box<Pen> penBox = new Box<>();
        //ItemSupplier<Pen> supplier = new PenSupplier();
        ItemSupplier<Pen> supplier = () -> new Pen();

        //BoxUtility.fillBox(penBox, supplier);
        //BoxUtility.fillBox(penBox, () -> new Pen());
        BoxUtility.fillBox(penBox, Pen::new);

        Assertions.assertFalse(penBox.isEmpty());
    }
}
