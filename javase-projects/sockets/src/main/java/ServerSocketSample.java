import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Arrays;

public class ServerSocketSample {
    public static void main(String[] args) throws IOException {
        try (ServerSocket serverSocket = new ServerSocket(8080)) {
            System.out.println("server is started");
            // block until a client is connected
            while (true) {
                try (Socket accept = serverSocket.accept()) {
                    accept.setSoTimeout(2000);
                    System.out.println("client is connected");
                    try (InputStream inputStream = accept.getInputStream()) {
                        byte[] data = new byte[8];
                        int i=0;
                        while(i < 8) {
                            int read = inputStream.read();// this will block until I receive 8 bytes
                            data[i++] = (byte) read;
                        }

                        try (OutputStream outputStream = accept.getOutputStream()) {
                            System.out.println("received bytes is: " + Arrays.toString(data));
                            outputStream.write(1);
                            outputStream.flush();
                        }
                    }
                    System.out.println("done");
                }
            }
        }
    }
}
