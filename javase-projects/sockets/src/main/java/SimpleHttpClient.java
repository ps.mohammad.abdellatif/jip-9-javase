import java.io.*;
import java.net.Socket;

public class SimpleHttpClient {

    /*
GET / HTTP/1.1
Host: www.progressoft.com

            */

    public static void main(String[] args) throws IOException {
        try (Socket socket = new Socket("www.google.com", 80)) {
            OutputStream outputStream = socket.getOutputStream();
            InputStream inputStream = socket.getInputStream();
            PrintWriter writer = new PrintWriter(outputStream);
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

            writer.println("GET / HTTP/1.1");
            writer.println("Host: www.google.com");
            writer.println();
            writer.flush();

            String line;
            while ((line = reader.readLine()) != null) {
                System.out.println(line);
            }

        }
    }
}
