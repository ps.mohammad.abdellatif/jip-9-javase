import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

public class ClientSocket {

    public static void main(String[] args) throws IOException {
        try (Socket socket = new Socket("www.phi01tech.com", 8080)) {
            OutputStream outputStream = socket.getOutputStream();
            InputStream inputStream = socket.getInputStream();
            System.out.println("connected, send 8 bytes");
            byte[] bytes = new byte[]{10, 11, 14, 13, 14, 15, 16};
            outputStream.write(bytes);
            outputStream.flush();

            System.out.println("message sent, receive one byte for acknowledgment");
            int read = inputStream.read();
            System.out.println("read byte: " + (byte) read);
        }
    }
}
