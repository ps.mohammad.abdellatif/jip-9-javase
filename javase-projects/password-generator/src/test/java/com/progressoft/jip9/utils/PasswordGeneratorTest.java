package com.progressoft.jip9.utils;

import org.junit.jupiter.api.*;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PasswordGeneratorTest {

    private static final List<Character> DIGITS = new ArrayList<>();
    private static final List<Character> ALPHABETS = new ArrayList<>();
    private static final List<Character> SPECIAL = new ArrayList<>();

    private List<SelectionOption> options;

    @BeforeAll
    public static void setupCharacters() {
        for (int i = '0'; i <= '9'; i++) {
            DIGITS.add((char) i);
        }
        for (int i = 'A'; i <= 'Z'; i++) {
            ALPHABETS.add((char) i);
        }
        SPECIAL.add('%');
        SPECIAL.add('$');
        SPECIAL.add('#');
        SPECIAL.add('_');
    }

    @BeforeEach
    public void setupOptions() {
        SelectionOption alphabets = new SelectionOption(ALPHABETS, 2);
        SelectionOption digits = new SelectionOption(DIGITS, 4);
        SelectionOption special = new SelectionOption(SPECIAL, 2);

        options = Arrays.asList(alphabets, digits, special);
    }

    @Test
    public void givenPasswordGeneratorWithRandomlySelection_whenGeneratePassword_thenReturnPasswordAccordingToRules() {
        PasswordGenerator generator = new PasswordGenerator(new RandomlySelector(), options);
        testGenerator(generator);
    }

    @RepeatedTest(1000)
    public void givenPasswordGeneratorWithSequentiallySelection_whenGeneratePassword_thenReturnPasswordAccordingToRules() {
        SelectionOption alphabets = new SelectionOption(ALPHABETS, 2);
        SelectionOption digits = new SelectionOption(DIGITS, 4);
        SelectionOption special = new SelectionOption(SPECIAL, 2);

        PasswordGenerator generator = new PasswordGenerator(new SequentiallySelector(), alphabets, digits, special);
        testGenerator(generator);
    }

    private void testGenerator(PasswordGenerator generator) {
        String password = generator.generate();

        Assertions.assertNotNull(password, "returned password was null");
        Assertions.assertEquals(8, password.length(), "returned password length was not 8");
        Assertions.assertTrue(containsAllowedCharacters(password), "returned password contains illegal character");

        Assertions.assertEquals(4, countUniqueMatches("[0-9]", password), "returned password does not contain 4 unique digits");
        Assertions.assertEquals(2, countUniqueMatches("[A-Z]", password), "returned password does not contain 2 unique alphabets");
        Assertions.assertEquals(2, countUniqueMatches("[%$#_]", password), "returned password does not contain 2 unique symbols");

        Assertions.assertTrue(isShuffled(password), "returned password is not shuffled");
        String password2 = generator.generate();
        Assertions.assertNotEquals(password, password2, "Two identical passwords were generated");
    }

    private boolean isShuffled(String password) {
        return !(doesMatch("[0-9]{4}", password) && doesMatch("[A-Z]{2}", password));
    }

    private boolean containsAllowedCharacters(String password) {
        return !doesMatch("[^0-9A-Z%$#_]", password);
    }

    private boolean doesMatch(String pattern, String input) {
        return Pattern.compile(pattern).matcher(input).find();
    }

    private int countUniqueMatches(String pattern, String input) {
        int ret = 0;
        Matcher matcher = Pattern.compile(pattern).matcher(input);
        Set<String> matched = new HashSet<>();
        while (matcher.find())
            if (matched.add(matcher.group()))
                ++ret;
        return ret;
    }
}
