package com.progressoft.jip9.utils;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Array;
import java.util.*;

public class SelectionOptionTest {

    @Test
    public void givenNullCharacters_whenConstruct_thenFail() {
        NullPointerException thrown = Assertions.assertThrows(NullPointerException.class,
                () -> new SelectionOption(null, -1));
        Assertions.assertEquals("null characters", thrown.getMessage());
    }

    @Test
    public void givenListAndNegativeCount_whenConstruct_thenFail() {
        IllegalArgumentException e = Assertions.assertThrows(IllegalArgumentException.class,
                () -> new SelectionOption(Collections.emptyList(), -1));
        Assertions.assertEquals("negative count", e.getMessage());
    }

    @Test
    public void givenListOfCharactersAndCountGreaterThanOptionsCount_whenConstruct_thenFail() {
        List<Character> characters = Arrays.asList('a', 'b', 'c', 'd');
        int count = characters.size() + 1 + new Random().nextInt(10);
        IllegalArgumentException e = Assertions.assertThrows(IllegalArgumentException.class,
                () -> new SelectionOption(characters, count));
        Assertions.assertEquals("count is greater than options: " + characters.size(), e.getMessage());
    }

    @Test
    public void givenValidListAndCount_whenConstruct_ThenSuccess() {
        List<Character> options = Arrays.asList('a', 'b', 'c', 'd');
        int count = 2;

        SelectionOption option = new SelectionOption(options, count);
        Assertions.assertEquals(count, option.getCount(),"count is invalid");
        Assertions.assertEquals(options, option.getOptions());
    }
}
