package com.progressoft.jip9.utils;

import java.util.ArrayList;
import java.util.List;

public class PoorSelector implements CharactersSelector {
    @Override
    public List<Character> selectCharacters(List<SelectionOption> options) {
        List<Character> characters = new ArrayList<>();
        for (SelectionOption option : options) {
            for (int i = 0; i < option.getCount(); i++) {
                characters.add(option.getOptions().get(i));
            }
        }
        return characters;
    }
}
