package com.progressoft.jip9.utils;

import java.util.Arrays;
import java.util.List;

public class PasswordGenerator {
    private final List<SelectionOption> options;

    // Strategy design pattern
    // Favor composition over inheritance
    // Encapsulate what varies (identify changing parts of the application, encapsulate it)
    private CharactersSelector charactersSelector;

    public PasswordGenerator(ShuffleType type, List<SelectionOption> options) {
        // TODO add unit test to verify options
        this.options = options;
        setShuffleType(type);
    }

    // Dependency injection
    public PasswordGenerator(CharactersSelector selector, List<SelectionOption> options) {
        this.options = options;
        this.charactersSelector = selector;
    }


    public PasswordGenerator(CharactersSelector selector, SelectionOption... options) {
        this.options = Arrays.asList(options);
        this.charactersSelector = selector;
    }

    private void setShuffleType(ShuffleType type) {
        if (type == ShuffleType.RANDOM) {
            charactersSelector = new RandomlySelector();
        } else {
            charactersSelector = new SequentiallySelector();
        }
    }

    public String generate() {
        List<Character> passwordCodePoints = charactersSelector.selectCharacters(options);
        return toPasswordString(passwordCodePoints);
    }

    private String toPasswordString(List<Character> passwordCodePoints) {
        StringBuilder password = new StringBuilder();
        for (Character c : passwordCodePoints) {
            password.append(c);
        }
        return password.toString();
    }
}
