package com.progressoft.jip9.utils;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class RandomlySelector implements CharactersSelector {

    private Random random = new SecureRandom();

    @Override
    public List<Character> selectCharacters(List<SelectionOption> options) {
        List<Character> passwordCodePoints = new ArrayList<>();
        for (SelectionOption option : options) {
            selectUniqueCodePoints(option.getOptions(), passwordCodePoints, option.getCount());
        }

        Collections.shuffle(passwordCodePoints, random);
        return passwordCodePoints;
    }

    private void selectUniqueCodePoints(List<Character> source, List<Character> dest, int count) {
        List<Character> sourceCopy = new ArrayList<>(source);
        for (int i = 0; i < count; ++i) {
            int chosenIndex = random.nextInt(sourceCopy.size());
            Character chosen = sourceCopy.remove(chosenIndex);
            dest.add(chosen);
        }
    }
}