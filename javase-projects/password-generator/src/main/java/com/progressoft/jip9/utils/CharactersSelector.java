package com.progressoft.jip9.utils;

import java.util.List;

public interface CharactersSelector {

    List<Character> selectCharacters(List<SelectionOption> options);
}