package com.progressoft.jip9.utils;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class SequentiallySelector implements CharactersSelector {
    private Random random = new SecureRandom();

    @Override
    public List<Character> selectCharacters(List<SelectionOption> options) {
        List<Selection> selections = prepareSelections(options);

        ArrayList<Character> characters = new ArrayList<>();
        int i = 0;
        while (!selections.isEmpty()) {
            int index = i++ % selections.size();
            Selection selection = selections.get(index);
            characters.add(selection.selectNext());
            if (!selection.hasMore())
                selections.remove(index);
        }

        return characters;
    }

    private List<Selection> prepareSelections(List<SelectionOption> options) {
        List<Selection> selections = new ArrayList<>();
        for (SelectionOption option : options) {
            selections.add(new Selection(option.getOptions(), option.getCount()));
        }
        Collections.shuffle(selections, random);
        return selections;
    }

    private class Selection {
        private List<Character> options;
        private int count;

        public Selection(List<Character> options, int count) {
            this.options = new ArrayList<>(options);
            this.count = count;
        }

        public boolean hasMore() {
            return count > 0;
        }

        public char selectNext() {
            if (!hasMore())
                throw new IllegalStateException("no more selections");
            Character chosen = options.remove(random.nextInt(options.size()));
            count--;
            return chosen;
        }
    }

}