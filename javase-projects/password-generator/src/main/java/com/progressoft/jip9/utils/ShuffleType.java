package com.progressoft.jip9.utils;

public enum ShuffleType {
    RANDOM , SEQUENTIALLY
}
