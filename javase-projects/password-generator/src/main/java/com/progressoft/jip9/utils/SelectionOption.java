package com.progressoft.jip9.utils;

import java.util.ArrayList;
import java.util.List;

public class SelectionOption {
    private final List<Character> options;
    private final int count;

    public SelectionOption(List<Character> options, int count) {
        if (options == null) {
            throw new NullPointerException("null characters");
        }
        failIfInvalidCount(options, count);
        this.count = count;
        this.options = new ArrayList<>(options);
    }

    private void failIfInvalidCount(List<Character> options, int count) {
        if (count < 0)
            throw new IllegalArgumentException("negative count");
        if (count > options.size())
            throw new IllegalArgumentException("count is greater than options: " + options.size());
    }

    public int getCount() {
        return count;
    }

    public List<Character> getOptions() {
        return new ArrayList<>(options);
    }
}
