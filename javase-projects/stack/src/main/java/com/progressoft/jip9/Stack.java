package com.progressoft.jip9;

import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class Stack<ITEM> implements Iterable<ITEM> {
    private int capacity;
    private ITEM stack[];
    private int top;
    private int modifications;

    //constructor
    public Stack(int capacity) {
        top = -1;
        stack = (ITEM[]) new Object[capacity];
        this.capacity = capacity;
    }

    //return the top element of the stack
    public ITEM peek() {
        if (top < 0) {
            throw new IllegalStateException("The stack is empty");
        }
        return stack[top];
    }

    //push method
    public void push(ITEM x) {
        if (top >= (capacity - 1))
            throw new IllegalStateException("The sack is full!");
        stack[++top] = x;
        modifications++;
    }

    //pup method
    public ITEM pop() {
        if (top < 0)
            throw new IllegalStateException("The stack is empty!");
        ITEM x = stack[top];
        stack[top--] = null;
        modifications++;
        return x;
    }

    @Override
    public Iterator<ITEM> iterator() {
        return new IteratorForStack<>();
    }

    private class IteratorForStack<ITEM> implements Iterator<ITEM> {
        int pointer = top;
        int modifications = Stack.this.modifications;

        @Override
        public boolean hasNext() {
            return pointer >= 0;
        }

        @Override
        public ITEM next() {
            if (!hasNext())
                throw new NoSuchElementException("There are no more elements");
            if (this.modifications != Stack.this.modifications)
                throw new ConcurrentModificationException();
            return (ITEM) stack[pointer--];
        }
    }

}
