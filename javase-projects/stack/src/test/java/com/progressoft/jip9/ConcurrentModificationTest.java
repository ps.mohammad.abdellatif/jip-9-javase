package com.progressoft.jip9;

import java.util.ArrayList;
import java.util.List;

public class ConcurrentModificationTest {

    public static void main(String[] args) {
        Stack<String> stack = new Stack<>(4);
        stack.push("A");
        stack.push("B");
        stack.push("C");
        stack.push("D");

        stack.forEach(System.out::println);
    }
}

