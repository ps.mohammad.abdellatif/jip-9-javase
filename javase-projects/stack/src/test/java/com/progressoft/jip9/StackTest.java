package com.progressoft.jip9;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class StackTest {

    @Test
    public void givenEmptyStack_whenPopFromStack_thenThrowEmptyStackException() {
        IllegalStateException exception = Assertions.assertThrows(IllegalStateException.class,
                () -> new Stack<>(3).pop());
        Assertions.assertEquals("The stack is empty!", exception.getMessage());
    }

    @Test
    public void givenValuesToPushInStack_whenStackFull_thenThrowFullStackException() {
        Stack<String> stack = new Stack<>(3);
        stack.push("Hello");
        stack.push("Welcome");
        stack.push("Oman");
        IllegalStateException exception = Assertions.assertThrows(IllegalStateException.class,
                () -> stack.push("Sunday"));
        Assertions.assertEquals("The sack is full!", exception.getMessage());
    }

    @Test
    public void givenStack_whenConstruct_thenSuccess() {
        Stack<Object> stack = new Stack<>(3);
        stack.push("Hello");
        Assertions.assertEquals("Hello", stack.peek());
        stack.push('A');
        Assertions.assertEquals('A', stack.peek());
        stack.push(50.5);
        Assertions.assertEquals(50.5, stack.peek());
    }

    @Test
    public void givenStackPushedWithData_whenIterateOverElements_thenSuccess() {
        Stack<String> stack = new Stack<>(4);
        stack.push("A");
        stack.push("B");
        stack.push("C");
        assertIterator(stack);
        assertIterator(stack);

        Iterator<String> iterator = stack.iterator();
        Assertions.assertSame("C", iterator.next());
        stack.pop();
        stack.push("E");
        Assertions.assertThrows(ConcurrentModificationException.class, () -> iterator.next());
    }

    private void assertIterator(Stack<String> stack) {
        Iterator<String> iterator = stack.iterator();
        Assertions.assertNotNull(iterator, "iterator is null");

        Assertions.assertTrue(iterator.hasNext(), "should return true for C");
        Assertions.assertSame("C", iterator.next());

        Assertions.assertTrue(iterator.hasNext(), "should return true for B");
        Assertions.assertSame("B", iterator.next());

        Assertions.assertTrue(iterator.hasNext(), "should return true for A");
        Assertions.assertSame("A", iterator.next());

        Assertions.assertFalse(iterator.hasNext(), "should return false");

        Assertions.assertThrows(NoSuchElementException.class, iterator::next);

    }

}
