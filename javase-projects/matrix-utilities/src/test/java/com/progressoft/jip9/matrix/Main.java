package com.progressoft.jip9.matrix;

public class Main {

    public static void main(String[] args) {
        // anonymous
//        Object obj = ()->{}; //incorrect
//        Shape shape = () -> {
//            return 0;
//        };// incorrect
        // it should be an interface
        // it should be functional
        CopyCondition condition = (int row, int col) -> {
            return false;
        };

    }


}
