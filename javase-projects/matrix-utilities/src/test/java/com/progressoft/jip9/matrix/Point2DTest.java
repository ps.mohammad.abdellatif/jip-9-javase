package com.progressoft.jip9.matrix;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class Point2DTest {

    @Test
    public void givenXandY_whenConstructPoint_thenPointIsConstructed() {
        int x = 10;
        int y = 3;
        Point2D point2D = new Point2D(x, y);
        Assertions.assertEquals(10, point2D.getX());
        Assertions.assertEquals(3, point2D.getY());
    }

    @Test
    public void givenTwoSimilarPoint_whenCheckEquality_thenShouldWorkAccordingToEqualsAndHashCodeRules() {
        Point2D p1 = new Point2D(2, 3);
        Point2D p2 = new Point2D(2, 3);

        Assertions.assertTrue(p1.equals(p1), "should return true");
        Assertions.assertFalse(p1.equals(null), "should return false");
        Assertions.assertTrue(p1.equals(p2), "should return true");
        Assertions.assertEquals(p1.hashCode(), p2.hashCode(), "should have the same hashcode");
        Assertions.assertFalse(p1.equals("(2,3)"));

        Assertions.assertEquals("point(2,3)", p1.toString());
    }
}
