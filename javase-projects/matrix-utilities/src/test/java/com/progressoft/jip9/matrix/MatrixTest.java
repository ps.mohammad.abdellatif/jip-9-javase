package com.progressoft.jip9.matrix;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class MatrixTest {

    @Test
    void givienValidMatrix_whenNewMatrix_thenAccept() {
        int[][] array = new int[][]{{1, 5, 6},
                {3, 9, 7},
                {3, 5, 9}};
        Matrix matrix = new Matrix(array);
        array[0][0] = 10;
        Assertions.assertEquals(3, matrix.getRows(), "invalid numbers of rows");
        Assertions.assertEquals(3, matrix.getCols(), "invalid numbers of columns");
        Assertions.assertEquals(3, matrix.getValue(1, 0), "invalid value");
        Assertions.assertEquals(7, matrix.getValue(1, 2), "invalid value");
        Assertions.assertEquals(1, matrix.getValue(0, 0), "invalid value");

    }

    @Test
    void givinNullRowMatrix_whenNewMatrix_thenFail() {
        int[][] array = new int[][]{{1, 5, 6},
                null,
                {3, 5, 9}};
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class,
                () -> new Matrix(array));
        Assertions.assertEquals("matrix contains null row(s)", exception.getMessage());
    }

    @Test
    void givinInconsistentMatrix_whenNewMatrix_thenFail() {
        int[][] array = new int[][]{{1, 5, 6},
                {7, 5, 10},
                {3, 9}};
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class,
                () -> new Matrix(array));
        Assertions.assertEquals("inconsistent matrix", exception.getMessage());
    }


    @Test
    public void givenTwoEqualsMatrices_whenEqualize_thenReturnTrue() {
        // if two objects are equals they should return the same hashcode
        // two objects could have the same hashcode but not equals
        // a.equals(a) true
        // a.equals(b) == b.equals(a)
        // a.equals(b) == a.equals(c) == b.equals(c)
        // a.equals(null) == false
        Matrix firstMatrix = new Matrix(new int[][]
                {{1, 5, 6},
                        {3, 9, 7},
                        {3, 5, 9}});
        Matrix secondMatrix = new Matrix(new int[][]
                {{1, 5, 6},
                        {3, 9, 7},
                        {3, 5, 9}});
        Matrix thirdMatrix = new Matrix(new int[][]
                {{1, 5, 6},
                        {3, 9, 7},
                        {3, 5, 10}});
        Matrix fourthMatrix = new Matrix(new int[][]
                {{1, 5, 6},
                        {3, 9, 7}});

        Assertions.assertTrue(firstMatrix.equals(secondMatrix), "matrices should be equal");
        Assertions.assertTrue(secondMatrix.equals(firstMatrix), "matrices should be equal");
        Assertions.assertTrue(firstMatrix.equals(secondMatrix) == secondMatrix.equals(firstMatrix));
        Assertions.assertEquals(firstMatrix.hashCode(), secondMatrix.hashCode(), "should have the same hashcode");

        Assertions.assertFalse(firstMatrix.equals(thirdMatrix), "should return false");
        Assertions.assertFalse(firstMatrix.equals(fourthMatrix), "should return false");

        Assertions.assertFalse(firstMatrix.equals(null), "equality to null should return false");
        Assertions.assertFalse(firstMatrix.equals(1));
        Assertions.assertFalse(firstMatrix.equals("hello"));
        Assertions.assertFalse(firstMatrix.equals(new Thread()));
    }


}
