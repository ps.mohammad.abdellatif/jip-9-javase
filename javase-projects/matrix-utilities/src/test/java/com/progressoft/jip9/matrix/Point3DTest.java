package com.progressoft.jip9.matrix;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class Point3DTest {
    @Test
    public void givenXandYandZ_whenConstructPoint_thenPointIsConstructed() {
        int x = 1, y = 2, z = 3;
        Point3D point = new Point3D(x, y, z);

        Assertions.assertEquals(x, point.getX(), "x was not as expected");
        Assertions.assertEquals(y, point.getY(), "y was not as expected");
        Assertions.assertEquals(z, point.getZ(), "z was not as expected");

    }

    @Test
    public void given2SimilarPoints_whenEquals_thenShouldWorkSameAsEqualAndHashRules() {
        Point3D p3d1 = new Point3D(1, 2, 3);
        Point3D p3d2 = new Point3D(1, 2, 3);
        Point3D p3d3 = new Point3D(1, 2, 4);
        Assertions.assertEquals(p3d1, p3d2);
        Assertions.assertEquals(p3d1.hashCode(), p3d2.hashCode(), "should have same hashcode");
        Assertions.assertTrue(p3d1.equals(p3d2), "should be same equal");
        Assertions.assertFalse(p3d1.equals(p3d3), "should not equal same");

        Point2D point2D = new Point2D(1, 2);
        Assertions.assertTrue(p3d2.equals(point2D) == point2D.equals(p3d2));
    }

}