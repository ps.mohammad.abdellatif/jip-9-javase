package com.progressoft.jip9.matrix;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class ReflectionSample {

    public static void main(String[] args) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        Point2D point2D = new Point2D(10, 3);
        System.out.println(point2D.getX());

        Class type = Point2D.class;
        Constructor constructor = type.getConstructor(Integer.TYPE, Integer.TYPE);
        Object instance = constructor.newInstance(10, 3);
        Method method = type.getMethod("getX");
        Object result = method.invoke(instance);
        System.out.println(result);

        Point2D p = (Point2D) instance;
        System.out.println(p.getX() + "," + p.getY());
    }
}
