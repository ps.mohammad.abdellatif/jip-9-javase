package com.progressoft.jip9.matrix;

public class Point2D {
    private final int x;
    private final int y;
    private final String desc;
    private final int hash;

    public Point2D(int x, int y) {
        this.x = x;
        this.y = y;
        desc = new StringBuilder("point(").append(this.x).append(",").append(this.y).append(")").toString();
        hash = calculateHash();
    }

    public final int getX() {
        return x;
    }

    public final int getY() {
        return y;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (o == null)
            return false;
        if (o.getClass() == Point2D.class) {// if passed object has the same type as me
            Point2D other = (Point2D) o;
            return other.x == this.x && other.y == this.y;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return hash;
    }

    @Override
    public String toString() {
        return desc;
    }

    private int calculateHash() {
        int result = 17;
        result += 13 * result + x;
        result += 13 * result + y;
        return result;
    }
}
