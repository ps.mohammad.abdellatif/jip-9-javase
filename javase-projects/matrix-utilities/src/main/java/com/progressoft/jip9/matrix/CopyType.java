package com.progressoft.jip9.matrix;

public enum CopyType {
    DIAGONAL, UPPER, LOWER;
}
