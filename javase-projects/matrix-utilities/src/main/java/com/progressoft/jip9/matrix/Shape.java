package com.progressoft.jip9.matrix;

public interface Shape {
    double PI = 3.14;

    // non-functional: has more than 2 abstract methods
    double area();

    double circumference();
}
