package com.progressoft.jip9.matrix;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

@Target({ElementType.TYPE, ElementType.METHOD})
public @interface Author {
    String name();

    String notes() default "n/a";
}
