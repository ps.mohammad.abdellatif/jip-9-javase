package com.progressoft.jip9.matrix;

public class Point3D extends Point2D {

    private final int z;

    public Point3D(int x, int y, int z) {
        super(x, y);
        this.z = z;
    }

    public int getZ() {
        return z;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this)
            return true;
        if (obj == null)
            return false;
        if (obj.getClass() == Point3D.class) {
            Point3D other = (Point3D) obj;
            return this.getX() == other.getX()
                    && this.getY() == other.getY()
                    && this.z == other.getZ();
        }
        return false;
    }
}
