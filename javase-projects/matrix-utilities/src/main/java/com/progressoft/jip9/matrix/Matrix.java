package com.progressoft.jip9.matrix;

import java.util.Arrays;

@Author(name = "u822")
@Version(1)
public class Matrix {

    private final int rows;
    private final int cols;
    private final int[][] matrix;

    public Matrix(int[][] array) {
        if (isNullRow(array)) {
            throw new IllegalArgumentException("matrix contains null row(s)");
        }
        if (isInconsistentMatrix(array)) {
            throw new IllegalArgumentException("inconsistent matrix");
        }
        this.rows = array.length;
        this.cols = array[0].length;
        this.matrix = copyArray(array);
    }

    @Author(name = "u822")
    public int getRows() {
        return this.rows;
    }

    public int getCols() {
        return this.cols;
    }

    public int getValue(int row, int col) {
        return this.matrix[row][col];
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Matrix)) {
            return false;
        }
        Matrix otherMatrix = (Matrix) object;
        if (this.getRows() != otherMatrix.getRows() || this.getCols() != otherMatrix.getCols()) {
            return false;
        }
        for (int row = 0; row < this.getRows(); row++) {
            for (int col = 0; col < this.cols; col++) {
                if (this.getValue(row, col) != otherMatrix.getValue(row, col)) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public int hashCode() {
        int result = 17;
        result += result * 13 + rows;
        result += result * 13 + cols;
        result += result * 13 + Arrays.deepHashCode(matrix);
        return result;
    }

    private int[][] copyArray(int[][] array) {
        int[][] temp = new int[array.length][];
        for (int i = 0; i < array.length; i++) {
            temp[i] = Arrays.copyOf(array[i], array[i].length);
        }
        return temp;
    }

    private boolean isInconsistentMatrix(int[][] array) {
        for (int i = 0; i < array.length; i++) {
            if (array[0].length != array[i].length) {
                return true;
            }
        }
        return false;
    }

    private boolean isNullRow(int[][] array) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] == null) {
                return true;
            }
        }
        return false;
    }


}
