package com.progressoft.jip9.matrix;

@FunctionalInterface
@Version(value = 2, minor = 0)
public interface CopyCondition {

    static CopyCondition isDiagonal() {
        return (row, col) -> row == col;
    }

    static CopyCondition isLower() {
        return (r, c) -> CopyCondition.isLower(r, (long) c);
    }

    static boolean isLower(long row, int col) {
        return row >= col;
    }

    static boolean isLower(int row, long col) {
        return row >= col;
    }

    static CopyCondition isUpper() {
        return CopyCondition::isUpper;
    }

    static boolean isUpper(int row, int col) {
        return row <= col;
    }


    // functional interfaces (provide one single abstract function)
    boolean doCopy(int row, int col);
}
