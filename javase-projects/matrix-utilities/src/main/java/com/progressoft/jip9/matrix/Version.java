package com.progressoft.jip9.matrix;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
public @interface Version {
    int value();

    int minor() default -1;
}
