package com.progressoft.jip9.matrix;

import java.util.Objects;

public class MatrixUtility implements Utility {

    public static int[][] sum(int[][] left, int[][] right) {
        failIfInvalidMatrices(left, right);
        // happy scenario
        return doSum(left, right);
    }

    public static int[][] scale(int[][] matrix, int scalar) {
        int[][] result = new int[matrix.length][matrix[0].length];
        for (int i = 0; i < result.length; i++) {
            for (int j = 0; j < result[i].length; j++) {
                result[i][j] = scalar * matrix[i][j];
            }
        }
        return result;
    }

    public static int[][] transpose(int[][] matrix) {
        failIfInvalidMatrix(matrix, "");

        int[][] result = new int[matrix[0].length][matrix.length];
        for (int row = 0; row < matrix.length; row++) {
            for (int col = 0; col < matrix[row].length; col++) {
                result[col][row] = matrix[row][col];
            }
        }
        return result;
    }

    public static int[][] sub(int[][] matrix, int rowToRemove, int colToRemove) {
        validateForSub(matrix, rowToRemove, colToRemove);
        return doSub(matrix, rowToRemove, colToRemove);
    }


    private static int[][] doSum(int[][] left, int[][] right) {
        int[][] result = new int[left.length][left[0].length];
        for (int row = 0; row < left.length; row++)
            for (int col = 0; col < result[row].length; col++)
                result[row][col] = left[row][col] + right[row][col];

        return result;
    }

    private static void failIfInvalidMatrices(int[][] left, int[][] right) {
        failIfInvalidMatrix(left, "first");
        failIfInvalidMatrix(right, "second");

        if (!isSameSize(left, right))
            throw new IllegalArgumentException("first and second matrices are of different sizes");
    }

    private static boolean isSameSize(int[][] left, int[][] right) {
        return left.length == right.length && left[0].length == right[0].length;
    }

    private static void failIfInvalidMatrix(int[][] left, String name) {
        name = name.isEmpty() || name == null ? "" : name + " ";
        for (int i = 0; i < left.length; i++) {
            if (left[i] == null)
                throw new IllegalArgumentException(name + "matrix contains null row(s)");
            if (left[0].length != left[i].length)
                throw new IllegalArgumentException("inconsistent rows in " + name + "matrix");
        }
    }

    private static void validateForSub(int[][] matrix, int rowToRemove, int colToRemove) {
        if (Objects.isNull(matrix))
            throw new NullPointerException("null matrix");
        if (isEmptyMatrix(matrix))
            throw new IllegalArgumentException("empty matrix");
        failIfInvalidMatrix(matrix, "");

        if (!isRowsWithinBounds(matrix, rowToRemove)) {
            throw new IllegalArgumentException("Rows out of bound");
        }
        if (!isColumnWithinBounds(matrix, colToRemove)) {
            throw new IllegalArgumentException("Column index out of bound");
        }
    }

    private static int[][] doSub(int[][] matrix, int rowToRemove, int colToRemove) {
        int[][] resultMatrix = new int[matrix.length - 1][matrix[0].length - 1];
        int targetRow = -1;
        for (int row = 0; row < matrix.length; row++) {
            if (row == rowToRemove) {
                continue;
            }
            targetRow++;
            int targetCol = -1;
            for (int col = 0; col < matrix[0].length; col++) {
                if (col == colToRemove) {
                    continue;
                }
                resultMatrix[targetRow][++targetCol] = matrix[row][col];
            }
        }
        return resultMatrix;
    }

    private static boolean isColumnWithinBounds(int[][] matrix, int col) {
        return col < matrix[0].length && col >= 0;
    }

    private static boolean isRowsWithinBounds(int[][] matrix, int row) {
        return row < matrix.length && row >= 0;
    }

    private static boolean isEmptyMatrix(int[][] matrix) {
        return matrix.length == 0;
    }

    public static Matrix scale(Matrix matrix, int scalar) {
        int[][] result = new int[matrix.getRows()][matrix.getCols()];
        for (int row = 0; row < matrix.getRows(); row++) {
            for (int col = 0; col < matrix.getCols(); col++) {
                result[row][col] = matrix.getValue(row, col) * scalar;
            }
        }
        return new Matrix(result);
    }

    public static Matrix transpose(Matrix matrix) {
        int[][] resultMatrix = new int[matrix.getCols()][matrix.getRows()];
        for (int row = 0; row < matrix.getRows(); row++) {
            for (int col = 0; col < matrix.getCols(); col++) {
                resultMatrix[col][row] = matrix.getValue(row, col);
            }
        }
        return new Matrix(resultMatrix);
    }

    public static int determinant(Matrix matrix) {
        throwIfNotSquare(matrix);
        return determinant(matrix, matrix.getRows());
    }

    public static Matrix copySquareMatrix(Matrix matrix, CopyType copyType) {
        throwIfNotSquare(matrix);
        if (copyType == null) {
            throw new IllegalArgumentException("Error, Invalid copy Type!!");
        }
        return doDiagonal(matrix, copyType);
    }

    public static Matrix upper(Matrix matrix) {
        //return copyMatrix(matrix, (row, col) -> row <= col);
        return copyMatrix(matrix, CopyCondition::isUpper);
    }

    private static void throwIfNotSquare(Matrix matrix) {
        if (matrix.getRows() != matrix.getCols())
            throw new IllegalArgumentException("Error, It's not a square Matrix !!");
    }

    private static Matrix doDiagonal(Matrix matrix, CopyType copyType) {
        CopyCondition condition = CopyConditionFactory.getCopyCondition(copyType);
        return copyMatrix(matrix, condition);
    }

    private static Matrix copyMatrix(Matrix matrix, CopyCondition condition) {
        int[][] result = new int[matrix.getRows()][matrix.getCols()];
        for (int row = 0; row < matrix.getRows(); row++) {
            for (int col = 0; col < matrix.getCols(); col++) {
                if (condition.doCopy(row, col))
                    result[row][col] = matrix.getValue(row, col);
            }
        }
        return new Matrix(result);
    }

    private static int determinant(Matrix matrix, int dimension) {
        if (dimension == 1) {
            return matrix.getValue(0, 0);
        }
        int determinant = 0;
        int sign = 1;
        for (int i = 0; i < dimension; i++) {
            Matrix sub = subMatrix(matrix, 0, i);
            determinant += sign * matrix.getValue(0, i) * determinant(sub, dimension - 1);
            sign = -sign;
        }
        return determinant;
    }


    public static Matrix subMatrix(Matrix matrix, int rowToRemove, int colToRemove) {
        validateRowAndCol(matrix, rowToRemove, colToRemove);
        int[][] result = new int[matrix.getRows()][matrix.getCols()];
        int fillingRow = 0;
        for (int i = 0; i < matrix.getRows(); i++) {
            if (i == rowToRemove)
                continue;
            int fillingCol = 0;
            for (int j = 0; j < matrix.getCols(); j++) {
                if (j == colToRemove)
                    continue;
                result[fillingRow][fillingCol++] = matrix.getValue(i, j);
            }
            fillingRow++;
        }
        return new Matrix(result);
    }

    private static void validateRowAndCol(Matrix matrix, int rowToRemove, int colToRemove) {
        if (rowToRemove >= matrix.getRows() || colToRemove >= matrix.getCols())
            throw new IllegalArgumentException("Ops, The n-by-m is greater than the matrix!!");

        if (rowToRemove < 0 || colToRemove < 0)
            throw new IllegalArgumentException("Ops, rowToRemove or colToRemove are in minus!!");
    }

}
