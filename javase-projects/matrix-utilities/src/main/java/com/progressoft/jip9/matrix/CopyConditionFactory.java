package com.progressoft.jip9.matrix;

// Factory: factory method (sub-types decide the object to be created)
// Factory: abstract factory (to construct a family of related types, i.e: UI components)
public class CopyConditionFactory {

    public static CopyCondition getCopyCondition(CopyType copyType) {
        // anonymous inner classes
        switch (copyType) {
            case DIAGONAL:
                // inferring 100%
                return CopyCondition.isDiagonal();
            case UPPER:
                return CopyCondition::isUpper;
            case LOWER:
                return CopyCondition.isLower();
        }
        throw new IllegalArgumentException("unknown copy type: " + copyType);
    }

    private interface CellPredicate {
        boolean match(int row, int col);
    }

    static {
        CopyCondition copyCondition = new CopyCondition() {
            @Override
            public boolean doCopy(int row, int col) {
                return row == col;
            }
        };
        copyCondition = (int row, int col) -> {
            return row == col;
        };
        copyCondition = (int row, int col) -> row == col;
        copyCondition = (row, col) -> row == col;
        // method reference
//        copyCondition = CopyCondition::isLower;
//        CellPredicate predicate = CopyCondition::isLower;
    }
}
