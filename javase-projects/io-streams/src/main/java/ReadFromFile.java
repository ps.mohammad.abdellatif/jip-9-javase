import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class ReadFromFile {

    public static void main(String[] args) throws IOException {
        File file = new File("./myname.txt");

        try (FileInputStream fis = new FileInputStream(file);) {
            int read;
            while ((read = fis.read()) != -1) {// end of file
                byte b = (byte) read;
                char c = (char) b;
                System.out.print(c);
            }

        }
    }
}
