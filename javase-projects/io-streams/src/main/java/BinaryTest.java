import java.io.UnsupportedEncodingException;
import java.util.Arrays;

public class BinaryTest {

    public static void main(String[] args) throws UnsupportedEncodingException {
        String name = "سائىى";
        byte[] bytes = name.getBytes("cp1256");// UTF-8
        System.out.println(bytes.length);
        System.out.println(Arrays.toString(bytes));
        String name2 = new String(bytes, "cp1256");
        System.out.println(name2);
    }
}
