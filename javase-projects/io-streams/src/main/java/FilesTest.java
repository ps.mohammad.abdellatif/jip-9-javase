import java.io.BufferedWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;

public class FilesTest {

    public static void main(String[] args) throws IOException {
        Path path = Paths.get(".", "dir1");
        Path textFile = path.resolve("sample.txt");
        if (Files.notExists(path)) {
            Files.createDirectory(path);
        }
        try (BufferedWriter writer = Files.newBufferedWriter(textFile, StandardOpenOption.APPEND)) {
            PrintWriter printWriter = new PrintWriter(writer);
            printWriter.println("hello from text dfd file");
        }

        List<String> lines = Files.readAllLines(textFile);
        System.out.println(lines);
    }
}
