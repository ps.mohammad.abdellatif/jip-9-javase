import java.io.File;
import java.io.IOException;

public class FileTest {

    public static void main(String[] args) throws IOException {
        File file = new File("/home/u822/jip9/sample.txt");
        // means nothing, pointer/home/u822/jip9 for a file/directory that could exists or not
        boolean exists = file.exists();
        System.out.println("exists: " + exists);
        if(!file.exists()) {
            file.createNewFile();
        }
        File dir = new File(file.getParentFile(),"dir1");
        // /home/u822/jip9/dir1
        if(!dir.exists()) {
            System.out.println("create directories");
            dir.mkdir();
        }
        System.out.println("done");
        long length = file.length();
        System.out.println("size in bytes: " + length);
    }
}
