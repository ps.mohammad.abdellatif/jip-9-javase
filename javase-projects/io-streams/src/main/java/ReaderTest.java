import java.io.*;

public class ReaderTest {

    public static void main(String[] args) throws IOException {
        File file = new File("./info.txt");
        try (InputStream fis = new FileInputStream(file)) {
            Reader reader = new InputStreamReader(fis, "UTF-8");
            BufferedReader br = new BufferedReader(reader);
            String line;
            while ((line = br.readLine()) != null) {
                System.out.println(line);
            }
//            long length = file.length();
//            for (int i = 0; i < length; i++) {
//                System.out.println((char) fis.read());
//            }
        }
    }
}
