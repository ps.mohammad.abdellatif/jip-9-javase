import java.nio.file.Path;
import java.nio.file.Paths;

public class PathsTest {

    public static void main(String[] args) {
        // ./dir1
        String home = System.getProperty("user.home");
        Path path = Paths.get(".", "dir1");
        Path homePath = Paths.get(home);

        System.out.println(homePath);
        System.out.println(path);

        Path absolutePath = path.toAbsolutePath().normalize();
        System.out.println(path);
        System.out.println(absolutePath);

        Path root = absolutePath.getRoot();
        Path atIndex = absolutePath.getName(2);
        Path fileName = absolutePath.getFileName();
        System.out.println(root);
        System.out.println(atIndex);
        System.out.println(fileName);

        System.out.println("to move from: ");
        System.out.println(absolutePath);
        System.out.println("to: ");
        System.out.println(homePath);
        Path relativize = absolutePath.relativize(homePath);
        System.out.println(relativize);


    }
}
