import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class WriteToFile {

    public static void main(String[] args) throws IOException {
        File file = new File("." + File.separator + "myname.txt");

        String name = "mohammad abdellatif";
        try (FileOutputStream fos = new FileOutputStream(file, true)) {
            byte[] bytes = name.getBytes();
            fos.write(bytes);
            fos.flush();
        }

        System.out.println("done");
    }
}
