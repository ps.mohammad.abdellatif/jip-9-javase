import java.io.*;

public class WritersTest {

    public static void main(String[] args) {
        File file = new File("./info.txt");
        try (OutputStream fos = new FileOutputStream(file);
             OutputStreamWriter writer = new OutputStreamWriter(fos, "UTF-8")) {
            writer.write("hello world: ");
            writer.write("انه انا جئت اليكم");
            writer.flush();
            System.out.println("done");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
