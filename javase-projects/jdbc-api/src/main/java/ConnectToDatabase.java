import java.sql.*;
import java.util.Scanner;

public class ConnectToDatabase {

    public static void main(String[] args) {
        String url = "jdbc:mysql://localhost:3306/induction?serverTimezone=UTC";
        String username = "root";
        String password = "root";
        // Lookup for implementation of Driver in classpath
        try (Connection connection = DriverManager.getConnection(url, username, password)) {
            System.out.println("connected");
            try (Statement statement = connection.createStatement()) {
                Scanner scanner = new Scanner(System.in);
                while (true) {
                    System.out.print("SQL>");
                    String sql = scanner.nextLine();
                    if (sql.endsWith(";")) {
                        sql = sql.substring(0, sql.length() - 1);
                    }
                    if (sql.equalsIgnoreCase("exit"))
                        break;
                    if (sql.equalsIgnoreCase("start transaction")) {
                        connection.setAutoCommit(false);
                        continue;
                    }
                    if (sql.equalsIgnoreCase("commit")) {
                        connection.commit();
                        connection.setAutoCommit(true);
                        continue;
                    }
                    if (sql.equalsIgnoreCase("rollback")) {
                        connection.rollback();
                        connection.setAutoCommit(true);
                        continue;
                    }
                    executeSQL(statement, sql);
                }
            }
        } catch (SQLException e) {
            printFailure(e);
        }
    }

    private static void executeSQL(Statement statement, String sql) {
        try {
            boolean query = statement.execute(sql);
            if (!query) {
                printAffectedRows(statement);
            } else {
                printResultSet(statement);
            }
        } catch (SQLException e) {
            printFailure(e);
        }
    }

    private static void printFailure(SQLException e) {
        System.out.println("something went wrong: " + e.getMessage());
        System.out.println("SQLState: " + e.getSQLState());
        System.out.println("Error: " + e.getErrorCode());
    }

    private static void printResultSet(Statement statement) throws SQLException {
        try (ResultSet resultSet = statement.getResultSet()) {
            ResultSetMetaData metaData = resultSet.getMetaData();
            int columnCount = metaData.getColumnCount();
            for (int i = 1; i <= columnCount; i++) {
                int size = metaData.getColumnDisplaySize(i);
                System.out.printf("%-" + size + "s|", metaData.getColumnLabel(i));
            }
            System.out.println();
            while (resultSet.next()) {
                for (int i = 1; i <= columnCount; i++) {
                    int size = metaData.getColumnDisplaySize(i);
                    String label = metaData.getColumnLabel(i);
                    System.out.printf("%-" + size + "s|", resultSet.getString(label));
                }
                System.out.println();
            }
        }
    }

    private static void printAffectedRows(Statement statement) throws SQLException {
        int updateCount = statement.getUpdateCount();
        System.out.println(updateCount + " row were affected");
    }
}
