package com.progressoft.jip9.finance;

import java.math.BigDecimal;

public class ConvertRequest {

    private final String from;
    private final String to;
    private final BigDecimal amount;

    // TODO enhance this code
    public ConvertRequest(String from, String to, BigDecimal amount) {
        failIfInvalidCodes(from, to);
        failIfInvalidAmounts(amount);
        this.from = from;
        this.to = to;
        this.amount = amount;
    }

    private void failIfInvalidAmounts(BigDecimal amount) {
        if (amount == null) {
            throw new NullPointerException("null amount");
        }
        if (amount.signum() <= 0)
            throw new IllegalArgumentException("positive amount is expected");
    }

    private void failIfInvalidCodes(String from, String to) {
        if (from == null) {
            throw new NullPointerException("null from");
        }
        if (to == null) {
            throw new NullPointerException("null to");
        }
    }

    public String getFrom() {
        return from;
    }

    public String getTo() {
        return to;
    }

    public BigDecimal getAmount() {
        return amount;
    }
}
