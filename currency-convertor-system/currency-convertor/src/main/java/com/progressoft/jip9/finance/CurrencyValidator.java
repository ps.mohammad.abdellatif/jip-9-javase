package com.progressoft.jip9.finance;

public interface CurrencyValidator {
    boolean isValid(String currency);
}
