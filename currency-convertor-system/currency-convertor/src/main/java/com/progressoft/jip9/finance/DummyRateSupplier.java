package com.progressoft.jip9.finance;
import java.math.BigDecimal;

public class DummyRateSupplier implements RateSupplier {
    @Override
    public BigDecimal getRate(String from, String to) {
        if (from.equals("USD") && to.equals("JOD"))
            return new BigDecimal("0.71");
        if (from.equals("USD") && to.equals("YEN"))
            return new BigDecimal("107.06");
        throw new IllegalStateException("I don't have such rate");
    }
}