package com.progressoft.jip9.finance;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

// SOLID
// S: Single responsibility
// O: Open for extension closed for modification (favor composition over inheritance)
// L: Liskov principle (don't call us we will call you)
// I: Interface segregation
// D: Dependency injection
public class CurrencyConvertor {

    // Dependency Injection
    private final CurrencyValidator currencyValidator;
    private final RateSupplier rateSupplier;

    public CurrencyConvertor(CurrencyValidator currencyValidator, RateSupplier rateSupplier) {
        this.currencyValidator = currencyValidator;
        this.rateSupplier = rateSupplier;
    }

    public BigDecimal convert(ConvertRequest request) {
//        StringUtility.toUpper("name");
        String from = request.getFrom();
        String to = request.getTo();
        if (!currencyValidator.isValid(from))
            throw new IllegalArgumentException("invalid from code");
        if (!currencyValidator.isValid(to))
            throw new IllegalArgumentException("invalid to code");
        BigDecimal ratio = rateSupplier.getRate(request.getFrom(), request.getTo());
        return request.getAmount().multiply(ratio);
    }

}
