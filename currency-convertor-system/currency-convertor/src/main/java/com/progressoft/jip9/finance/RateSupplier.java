package com.progressoft.jip9.finance;

import java.math.BigDecimal;
import java.math.RoundingMode;

@FunctionalInterface
public interface RateSupplier {

    BigDecimal getRate(String from, String to);

    default RateSupplier reverse() { // utility
        RateSupplier base = this;
        return (f, t) -> {
            try {
                return base.getRate(f, t);
            } catch (IllegalStateException e) {
                return BigDecimal.ONE.divide(base.getRate(t, f), 3, RoundingMode.HALF_UP);
            }
        };
    }

    class ReverseSupplier implements RateSupplier {
        private final RateSupplier rateSupplier;
        private ReverseSupplier(RateSupplier rateSupplier) {
            this.rateSupplier = rateSupplier;
        }
        @Override
        public BigDecimal getRate(String from, String to) {
            try {
                return rateSupplier.getRate(from, to);
            } catch (IllegalStateException e) {
                return BigDecimal.ONE.divide(rateSupplier.getRate(to, from), 3, RoundingMode.HALF_UP);
            }
        }
    }
}
