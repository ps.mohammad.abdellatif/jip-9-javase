package com.progressoft.jip9.finance;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Random;

public class CurrencyConvertorTest {

    public static final String INVALID_CURRENCY = "UUU";
    private MockValidator currencyValidator = new MockValidator();

    // Mocking
    private class MockValidator implements CurrencyValidator {

        @Override
        public boolean isValid(String currency) {
            return !currency.equalsIgnoreCase(INVALID_CURRENCY);
        }
    }

    @Test
    public void givenInvalidFromCode_whenConvert_thenFail() {
        CurrencyConvertor convertor = new CurrencyConvertor((c) -> c.equalsIgnoreCase("JOD"), null);
        ConvertRequest request = new ConvertRequest(INVALID_CURRENCY, "JOD", BigDecimal.valueOf(100));
        // how to verify the invalidity of the code
        IllegalArgumentException thrown = Assertions.assertThrows(IllegalArgumentException.class,
                () -> convertor.convert(request));
        Assertions.assertEquals("invalid from code", thrown.getMessage());
    }

    @Test
    public void givenValidFromCodeAndInvalidToCode_whenConvert_thenFail() {
        CurrencyConvertor convertor = new CurrencyConvertor(c -> c.equalsIgnoreCase("USD"), null);
        ConvertRequest request = new ConvertRequest("USD", INVALID_CURRENCY, BigDecimal.valueOf(100));
        // how to verify the invalidity of the code
        IllegalArgumentException thrown = Assertions.assertThrows(IllegalArgumentException.class,
                () -> convertor.convert(request));
        Assertions.assertEquals("invalid to code", thrown.getMessage());
    }

    @RepeatedTest(10)
    public void givenValidRequest_whenConvert_thenSuccess() {
        BigDecimal amount = BigDecimal.valueOf(230.5);
        BigDecimal[] possibleRatios =
                {new BigDecimal("0.71"), new BigDecimal("1.41"), new BigDecimal("0.80")};
        BigDecimal ratio = possibleRatios[new Random().nextInt(possibleRatios.length)];

        ConvertRequest request = new ConvertRequest("USD", "JOD", amount);
        CurrencyConvertor convertor = new CurrencyConvertor(c -> true, (f, t) -> ratio);
        BigDecimal result = convertor.convert(request);

        Assertions.assertNotNull(result, "result is null");
        BigDecimal expected = ratio.multiply(amount);
        Assertions.assertEquals(0, expected.compareTo(result), "converted amount is not as expected");
    }


}
