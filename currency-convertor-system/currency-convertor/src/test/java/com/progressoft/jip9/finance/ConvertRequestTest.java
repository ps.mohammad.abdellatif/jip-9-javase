package com.progressoft.jip9.finance;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

public class ConvertRequestTest {

    @Test
    public void givenNullParameters_whenConstructConvertRequest_thenThrowException() {
        Exception thrown = Assertions.assertThrows(NullPointerException.class,
                () -> new ConvertRequest(null, "JOD", BigDecimal.valueOf(1.0)));
        Assertions.assertEquals("null from", thrown.getMessage());

        thrown = Assertions.assertThrows(NullPointerException.class,
                () -> new ConvertRequest("USD", null, BigDecimal.valueOf(1.0)));
        Assertions.assertEquals("null to", thrown.getMessage());

        thrown = Assertions.assertThrows(NullPointerException.class,
                () -> new ConvertRequest("USD", "JOD", null));
        Assertions.assertEquals("null amount", thrown.getMessage());

        thrown = Assertions.assertThrows(IllegalArgumentException.class,
                () -> new ConvertRequest("USD", "JOD", BigDecimal.valueOf(0)));
        Assertions.assertEquals("positive amount is expected", thrown.getMessage());

        thrown = Assertions.assertThrows(IllegalArgumentException.class,
                () -> new ConvertRequest("USD", "JOD", BigDecimal.valueOf(-10.89)));
        Assertions.assertEquals("positive amount is expected", thrown.getMessage());

    }

    @Test
    public void givenValidParameters_whenConstructConvertRequest_thenRequestIsConstructed() {
        String from = "USD";
        String to ="JOD";
        BigDecimal amount = BigDecimal.valueOf(1.0);
        ConvertRequest request = new ConvertRequest(from, to, amount);
        Assertions.assertEquals(from, request.getFrom());
        Assertions.assertEquals(to, request.getTo());
        Assertions.assertEquals(amount, request.getAmount());
    }
}
