package com.progressoft.jip9.bis.servlet;

import com.progressoft.jip9.bis.model.History;
import com.progressoft.jip9.finance.CSVStore;
import com.progressoft.jip9.finance.ConvertRequest;
import com.progressoft.jip9.finance.CurrencyConvertor;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

public class ConvertServlet extends HttpServlet {

    private final CurrencyConvertor currencyConvertor;
    private final CSVStore csvStore;
    private final HistoryStorage historyStorage;

    public ConvertServlet(CurrencyConvertor currencyConvertor, CSVStore csvStore, HistoryStorage historyStorage) {
        this.currencyConvertor = currencyConvertor;
        this.csvStore = csvStore;

        this.historyStorage = historyStorage;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<String> currencies = csvStore.getCurrencies();
        if (currencies.isEmpty()) {
            resp.sendError(HttpServletResponse.SC_SERVICE_UNAVAILABLE, "No currencies available");
            return;
        }
        req.setAttribute("currenciesList", currencies);
        List<History> histories = historyStorage.listHistory(req, resp);
        req.setAttribute("history", histories);
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("/WEB-INF/views/convert.jsp");
        requestDispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String from = req.getParameter("from");
        String to = req.getParameter("to");
        String amount = req.getParameter("amount");

        if (StringUtils.isAnyEmpty(from, to, amount)) {
            resp.sendError(HttpServletResponse.SC_BAD_REQUEST, "from, to, and amount parameters is required");
            return;
        }

        BigDecimal bigDecimalAmount = new BigDecimal(amount);
        ConvertRequest convertRequest = new ConvertRequest(from, to, bigDecimalAmount);
        BigDecimal converted = currencyConvertor.convert(convertRequest);
        BigDecimal rate = converted.divide(bigDecimalAmount, 3, BigDecimal.ROUND_CEILING);

        req.setAttribute("convertResult", converted);
        req.setAttribute("rate", rate);

        History history = new History(LocalDateTime.now(), from, to, bigDecimalAmount, rate, converted);
        List<History> histories = historyStorage.storeHistory(req, resp, history);
        req.setAttribute("history", histories);

        req.changeSessionId();

        RequestDispatcher requestDispatcher = req.getRequestDispatcher("/WEB-INF/views/ConvertResult.jsp");
        requestDispatcher.forward(req, resp);
    }

}
