package com.progressoft.jip9.bis.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class HistoryArray {

    public static final String TRANSACTION_SPLIT = "/";
    private final List<History> historyList = new ArrayList<>();

    public void add(History history) {
        historyList.add(history);
    }

    public List<History> getHistoryList() {
        return Collections.unmodifiableList(historyList);
    }

    public static HistoryArray getHistoryList(String query) {
        HistoryArray historyArray = new HistoryArray();
        String[] split = query.split(TRANSACTION_SPLIT);
        for (String s : split) {
            historyArray.add(History.getHistory(s));
        }
        return historyArray;
    }

    @Override
    public String toString() {
        StringBuilder total = new StringBuilder();
        for (int i = 0; i < historyList.size(); i++) {
            if (i + 1 == historyList.size())
                total.append(historyList.get(i).toString());
            else
                total.append(historyList.get(i).toString()).append(TRANSACTION_SPLIT);
        }
        return total.toString();
    }
}