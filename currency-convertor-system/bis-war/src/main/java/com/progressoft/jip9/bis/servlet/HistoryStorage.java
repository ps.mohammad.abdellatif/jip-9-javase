package com.progressoft.jip9.bis.servlet;

import com.progressoft.jip9.bis.model.History;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public interface HistoryStorage {

    List<History> storeHistory(HttpServletRequest req, HttpServletResponse resp, History history);

    List<History> listHistory(HttpServletRequest req, HttpServletResponse resp);
}
