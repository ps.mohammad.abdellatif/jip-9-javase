package com.progressoft.jip9.bis.restApi;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.progressoft.jip9.finance.CSVStore;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class CurrenciesServlet extends HttpServlet {

    private final CSVStore csvStore;
    private Gson gson;

    public CurrenciesServlet(CSVStore csvStore) {
        this.csvStore = csvStore;
    }

    @Override
    public void init() throws ServletException {
        GsonBuilder gb = new GsonBuilder();
        gb.setPrettyPrinting();
        gson = gb.create();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String pathInfo = req.getPathInfo();
        System.out.println(req.getServletPath());
        System.out.println(pathInfo);
        if (pathInfo == null || pathInfo.isEmpty()) {
            getCurrenciesList(resp);
        } else {
            getSpecificEntity(resp, pathInfo);
        }
    }

    private void getSpecificEntity(HttpServletResponse resp, String pathInfo) throws IOException {
        String currencyID = pathInfo.substring(1);
        if (!csvStore.getCurrencies().contains(currencyID)) {
            resp.sendError(HttpServletResponse.SC_NOT_FOUND);
            return;
        }
        Map<String, Object> entity = new HashMap<>();
        entity.put("code", currencyID);
        gson.toJson(entity, resp.getWriter());
    }

    private void getCurrenciesList(HttpServletResponse resp) throws IOException {
        List<String> currencies = csvStore.getCurrencies();
        resp.setContentType("application/json");
        Map<String, Object> result = new HashMap<>();
        result.put("currenciesList", currencies);
        gson.toJson(result, resp.getWriter());
    }
}
















