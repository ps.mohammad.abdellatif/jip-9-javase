package com.progressoft.jip9.bis.initializer;

import com.progressoft.jip9.bis.filters.DomainCheckFilter;
import com.progressoft.jip9.bis.filters.RequestTimeLogFilter;
import com.progressoft.jip9.bis.restApi.CurrenciesServlet;
import com.progressoft.jip9.bis.servlet.ConvertServlet;
import com.progressoft.jip9.bis.servlet.RateServlet;
import com.progressoft.jip9.bis.servlet.SessionHistoryStorage;
import com.progressoft.jip9.finance.*;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.*;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.EnumSet;
import java.util.Set;

public class BISInitializer implements ServletContainerInitializer {

    @Override
    public void onStartup(Set<Class<?>> c, ServletContext ctx) throws ServletException {
        CSVStore csvStore = prepareCsvStore(ctx);
        RateSupplier rateSupplier = new BISRateSupplier(csvStore);
        CurrencyValidator currencyValidator = new BISCurrenciesValidator(csvStore);
        CurrencyConvertor currencyConvertor = new CurrencyConvertor(currencyValidator, rateSupplier);

        registerRateServlet(ctx, rateSupplier);
        registerConvertServlet(ctx, currencyConvertor, csvStore);
        registerResetApiServlet(ctx, csvStore);

        registerRequestTimeLogFilter(ctx);
        registerDomainCheckFilter(ctx);
    }

    private void registerDomainCheckFilter(ServletContext ctx) {
        FilterRegistration.Dynamic domainCheckFilter = ctx.addFilter("domainCheckFilter", new DomainCheckFilter());
        domainCheckFilter.addMappingForUrlPatterns(EnumSet.of(DispatcherType.REQUEST), true, "/*");
    }

    private void registerRequestTimeLogFilter(ServletContext ctx) {
        FilterRegistration.Dynamic registration = ctx.addFilter("requestTimeLogFilter", new RequestTimeLogFilter());
        registration.addMappingForUrlPatterns(EnumSet.of(DispatcherType.REQUEST), true, "/*");
    }

    private void registerResetApiServlet(ServletContext ctx, CSVStore csvStore) {
        CurrenciesServlet resetApi = new CurrenciesServlet(csvStore);
        ServletRegistration.Dynamic servletRegistration = ctx.addServlet("currenciesCodsServlet", resetApi);
        servletRegistration.addMapping("/currenciesCods/*");
    }

    private void registerConvertServlet(ServletContext ctx, CurrencyConvertor currencyConvertor, CSVStore csvStore) {
        ConvertServlet convertServlet = new ConvertServlet(currencyConvertor, csvStore, new SessionHistoryStorage());
        ServletRegistration.Dynamic servletRegistration = ctx.addServlet("convertServlet", convertServlet);
        servletRegistration.addMapping("/convert");
    }

    private void registerRateServlet(ServletContext ctx, RateSupplier rateSupplier) {
        RateServlet servlet = new RateServlet(rateSupplier);
        ServletRegistration.Dynamic servletRegistration = ctx.addServlet("RateServlet", servlet);
        servletRegistration.addMapping("/rate");
    }

    private CSVStore prepareCsvStore(ServletContext ctx) throws ServletException {
        String bisRatesPath = ctx.getInitParameter("bis-path");
        if (StringUtils.isEmpty(bisRatesPath))
            throw new ServletException("bis-path context parameter is not defined");
        try {
            return new CSVStore(Paths.get(bisRatesPath));
        } catch (IOException e) {
            throw new ServletException("failed to initialize application", e);
        }
    }
}
