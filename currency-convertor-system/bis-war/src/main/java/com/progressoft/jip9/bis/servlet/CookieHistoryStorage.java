package com.progressoft.jip9.bis.servlet;

import com.progressoft.jip9.bis.model.History;
import com.progressoft.jip9.bis.model.HistoryArray;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.Serializable;
import java.util.List;

public class CookieHistoryStorage implements Serializable, HistoryStorage {

    @Override
    public List<History> storeHistory(HttpServletRequest req, HttpServletResponse resp, History history) {
        Cookie[] cookies = req.getCookies();
        HistoryArray historyArray = getHistoryArray(cookies);
        historyArray.add(history);
        Cookie cookie = new Cookie("history", historyArray.toString());
        resp.addCookie(cookie);
        return historyArray.getHistoryList();
    }

    @Override
    public List<History> listHistory(HttpServletRequest req, HttpServletResponse resp) {
        return getHistoryArray(req.getCookies()).getHistoryList();
    }

    private HistoryArray getHistoryArray(Cookie[] cookies) {
        HistoryArray historyArray = new HistoryArray();
        if (cookies == null)
            return historyArray;
        for (Cookie cookie : cookies) {
            if (cookie.getName().equals("history")) {
                String value = cookie.getValue();
                historyArray = HistoryArray.getHistoryList(value);
            }
        }
        return historyArray;
    }
}