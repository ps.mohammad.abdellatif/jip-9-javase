package com.progressoft.jip9.bis.model;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class History implements Serializable {

    public static final int FROM = 1;
    public static final int TO = 2;
    public static final int AMOUNT = 3;
    public static final int RATE = 4;
    public static final int TOTAL = 5;
    private final String time;
    private final String from;
    private final String to;
    private final BigDecimal amount;
    private final BigDecimal rate;
    private final BigDecimal total;

    public History(LocalDateTime time, String from, String to, BigDecimal amount, BigDecimal rate, BigDecimal total) {
        DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
        this.time = time.format(myFormatObj);
        this.from = from;
        this.to = to;
        this.amount = amount;
        this.rate = rate;
        this.total = total;
    }

    private History(String time, String from, String to, BigDecimal amount, BigDecimal rate, BigDecimal total) {
        this.time = time;
        this.from = from;
        this.to = to;
        this.amount = amount;
        this.rate = rate;
        this.total = total;
    }

    public static History getHistory(String s) {
        String[] split = s.split("\\+");
        String[] time = split[0].split("\\.");
        String dateTime = time[0] + " " + time[1].replace('*', ':');
        return new History(dateTime, split[FROM], split[TO], new BigDecimal(split[AMOUNT])
                , new BigDecimal(split[RATE]), new BigDecimal(split[TOTAL]));
    }

    public String getFrom() {
        return from;
    }

    public String getTo() {
        return to;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public String getTime() {
        return time;
    }

    @Override
    public String toString() {
        try {
            String[] dateTime = time.split(" ");
            return URLEncoder.encode(dateTime[0] + "." + dateTime[1].replace(':', '*') + " " + from + " " + to + " " + amount
                    + " " + rate + " " + total, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        }
    }
}
