package com.progressoft.jip9.bis.filters;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class DomainCheckFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpReq = (HttpServletRequest) request;
        String host = httpReq.getServerName();
        if (!host.equals("127.0.0.1")) {
            HttpServletResponse httpResp = (HttpServletResponse) response;
            httpResp.sendError(HttpServletResponse.SC_BAD_REQUEST, "Invalid domain");
            return;
        }

        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {

    }
}
