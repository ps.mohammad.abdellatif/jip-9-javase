package com.progressoft.jip9.bis.servlet;

import com.progressoft.jip9.finance.RateNotFoundException;
import com.progressoft.jip9.finance.RateSupplier;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;

public class RateServlet extends HttpServlet {

    private RateSupplier rateSupplier;

    public RateServlet(RateSupplier rateSupplier) {
        this.rateSupplier = rateSupplier;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String from = req.getParameter("from");
        String to = req.getParameter("to");
        if (StringUtils.isEmpty(from) || StringUtils.isEmpty(to)) {
            resp.sendError(HttpServletResponse.SC_BAD_REQUEST, "from and to parameters should be valid");
            return;
        }

        try {
            BigDecimal rate = rateSupplier.getRate(from, to);
            renderSuccessResponse(resp, from, to, rate);
        } catch (RateNotFoundException e) {
            resp.sendError(HttpServletResponse.SC_NOT_FOUND, "no rate found: " + from + " to " + to);
        }

    }

    private void renderSuccessResponse(HttpServletResponse resp, String from, String to, BigDecimal rate) throws IOException {
        resp.setContentType("text/html");
        PrintWriter writer = resp.getWriter();
        writer.append("<html><body>");
        writer.append("Rate information:");
        writer.append("<table>");
        writer.append("<tr>");
        writer.append("<th>From</th>");
        writer.append("<td>").append(from).append("</td>");
        writer.append("</tr>");
        writer.append("<tr>");
        writer.append("<th>To</th>");
        writer.append("<td>").append(to).append("</td>");
        writer.append("</tr>");
        writer.append("<tr>");
        writer.append("<th>Rate</th>");
        writer.append("<td>").append(rate.toString()).append("</td>");
        writer.append("</tr>");
        writer.append("</table>");
        writer.append("</body></html>");
    }
}
