package com.progressoft.jip9.bis.filters;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class RequestTimeLogFilter implements Filter {

    private static final Logger logger = LoggerFactory.getLogger(RequestTimeLogFilter.class);

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpReq = (HttpServletRequest) request;
        HttpServletResponse httpResp = (HttpServletResponse) response;
        logger.info("receive request: " + httpReq.getRequestURI());
        long start = System.currentTimeMillis();

        // proceed filter chain (next node could be the servlet)
        chain.doFilter(request, response);

        long end = System.currentTimeMillis();
        logger.info("request: " + httpReq.getRequestURI() + " took " + (end - start) + " milliseconds");
    }

    @Override
    public void destroy() {

    }
}
