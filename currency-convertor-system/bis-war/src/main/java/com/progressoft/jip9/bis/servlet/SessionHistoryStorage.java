package com.progressoft.jip9.bis.servlet;

import com.progressoft.jip9.bis.model.History;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SessionHistoryStorage implements Serializable, HistoryStorage {

    public List<History> storeHistory(HttpServletRequest req, HttpServletResponse resp, History history) {
        HttpSession session = req.getSession(true);
        List<History> historyList = getHistoryListFromSession(session);

        historyList.add(0, history);
        session.setAttribute("history", historyList);
        return Collections.unmodifiableList(historyList);
    }

    @Override
    public List<History> listHistory(HttpServletRequest req, HttpServletResponse resp) {
        return getHistoryListFromSession(req.getSession(true));
    }

    private List<History> getHistoryListFromSession(HttpSession session) {
        List<History> historyList;

        if (session.getAttribute("history") != null) {
            historyList = (List<History>) session.getAttribute("history");
        } else {
            historyList = new ArrayList<>();
        }
        return historyList;
    }
}