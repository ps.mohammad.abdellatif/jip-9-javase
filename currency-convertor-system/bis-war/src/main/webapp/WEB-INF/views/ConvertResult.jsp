<%@taglib prefix="bis" tagdir="/WEB-INF/tags" %>

<body>
<h4>Your Request Result</h4>
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-4">
            <table class="table table-dark">
                <tr>
                    <th>From</th>
                    <td>${param.from}</td>
                <tr>
                <tr>
                    <th>To</th>
                    <td>${param["to"]}</td>
                <tr>
                <tr>
                    <th>Amount</th>
                    <td>${param["amount"]}</td>
                <tr>
                <tr>
                    <th>Rate</th>
                    <td>${rate}</td>
                <tr>
                <tr>
                    <th>Converted</th>
                    <td>${requestScope.convertResult}</td>
                <tr>
            </table>
        </div>
    </div>
</div>

<bis:ConvertHistory />
