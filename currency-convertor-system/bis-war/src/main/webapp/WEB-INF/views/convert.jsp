<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="bis" tagdir="/WEB-INF/tags" %>


<body>
<form method="post" action="${pageContext.request.contextPath}/convert">
    <table>
        <tr>
            <td>From:</td>
            <td>
                <select id="from" name="from">
                </select>
            </td>
        </tr>
        <tr>
            <td>To:</td>
            <td><select id="to" name="to">
            </select></td>
        </tr>
        <tr>
            <td>Amount:</td>
            <td><input type="number" name="amount"></td>
        </tr>
        <tr>
            <td></td>
            <td><input type="submit" value="Convert"></td>
        </tr>
    </table>
</form>

<script>
    let from = document.getElementById('from');
    let to = document.getElementById('to');

    const url = '/bis/currenciesCods';

    fetch(url)
        .then(
            function (response) {
                if (response.status !== 200) {
                    console.warn('Looks like there was a problem. Status Code: ' +
                        response.status);
                    return;
                }

                response.json().then(function (data) {
                    let option;

                    for (let i = 0; i < data.currenciesList.length; i++) {
                        option = document.createElement('option');
                        option.text = data.currenciesList[i];
                        option.value = data.currenciesList[i];
                        from.add(option);
                    }
                    for (let i = 0; i < data.currenciesList.length; i++) {
                        option = document.createElement('option');
                        option.text = data.currenciesList[i];
                        option.value = data.currenciesList[i];
                        to.add(option);
                    }
                });
            }
        )
        .catch(function (err) {
            console.error('Fetch Error -', err);
        });

</script>

<bis:ConvertHistory isDark="false" />