<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@attribute name="isDark" type="java.lang.Boolean" required="false" %>

<c:if test="${requestScope.history ne null}">
<table class="${isDark ? 'table-dark' : 'table'}  table-striped" style="margin-top: 50px; margin-left: 10px; width: 98%">
    <thead>
    <tr>
        <th scope="col">Time</th>
        <th scope="col">From</th>
        <th scope="col">To</th>
        <th scope="col">Amount</th>
        <th scope="col">Rate</th>
        <th scope="col">Total</th>
    </tr>
    </thead>
    <tbody>
        <c:forEach items="${requestScope.history}" var="history">
            <tr>
                <td>${history.time}</td>
                <td>${history.from}</td>
                <td>${history.to}</td>
                <td>${history.amount}</td>
                <td>${history.rate}</td>
                <td>${history.total}</td>
            </tr>
        </c:forEach>
    </tbody>
    </c:if>
</table>