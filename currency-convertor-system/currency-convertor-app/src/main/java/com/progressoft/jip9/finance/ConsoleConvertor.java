package com.progressoft.jip9.finance;

import java.math.BigDecimal;
import java.util.Scanner;

public class ConsoleConvertor {

    private CurrencyConvertor convertor;

    public ConsoleConvertor(CurrencyConvertor convertor) {
        this.convertor = convertor;
    }

    public void run() {
        StringUtility.toUpper("sami");
        Scanner scanner = new Scanner(System.in);
        String from = scanner.next();
        String to = scanner.next();
        BigDecimal amount = scanner.nextBigDecimal();

        BigDecimal result = convertor.convert(new ConvertRequest(from, to, amount));
        System.out.println(result);
    }
}
