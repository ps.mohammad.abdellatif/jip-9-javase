package com.progressoft.jip9.finance;

import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Main {

    public static void main(String[] args) throws IOException {
        String bis = System.getProperty("bis.path");
        if (StringUtils.isBlank(bis)) {
            System.err.println("you need to pass the bis file location");
            return;
        }
        Path bisPath = Paths.get(bis);
        System.out.println(bisPath.toAbsolutePath());
        CSVStore store = new CSVStore(bisPath);
        CurrencyValidator validator = new BISCurrenciesValidator(store);
        RateSupplier rateSupplier = new BISRateSupplier(store);
        CurrencyConvertor convertor = new CurrencyConvertor(validator, rateSupplier);
        ConsoleConvertor consoleConvertor = new ConsoleConvertor(convertor);
        consoleConvertor.run();
    }

    private static class ToUpperSupplier implements RateSupplier {
        private RateSupplier supplier;

        private ToUpperSupplier(RateSupplier supplier) {
            this.supplier = supplier;
        }

        @Override
        public BigDecimal getRate(String from, String to) {
            return supplier.getRate(from.toUpperCase(), to.toUpperCase());
        }
    }

    private static class ReverseCheckSupplier implements RateSupplier {
        private final RateSupplier rateSupplier;

        private ReverseCheckSupplier(RateSupplier rateSupplier) {
            this.rateSupplier = rateSupplier;
        }

        @Override
        public BigDecimal getRate(String from, String to) {
            try {
                return rateSupplier.getRate(from, to);
            } catch (IllegalStateException e) {
                return BigDecimal.ONE.divide(rateSupplier.getRate(to, from), 3, RoundingMode.HALF_UP);
            }
        }
    }
}
