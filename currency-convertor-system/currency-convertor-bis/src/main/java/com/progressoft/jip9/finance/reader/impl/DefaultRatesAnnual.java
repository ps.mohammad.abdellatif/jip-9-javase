package com.progressoft.jip9.finance.reader.impl;

import com.progressoft.jip9.finance.reader.YearlyRates;

import java.util.List;

class DefaultRatesAnnual extends RatesBase<Integer> implements YearlyRates {


    public DefaultRatesAnnual(String[] record, Border annual, List<String> header) {
        super(record, annual, header);
    }

    @Override
    protected Integer getKey(String header) {
        return Integer.parseInt(header);
    }


}
