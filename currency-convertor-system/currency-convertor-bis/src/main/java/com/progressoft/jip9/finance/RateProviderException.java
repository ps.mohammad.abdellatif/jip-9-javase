package com.progressoft.jip9.finance;

public class RateProviderException extends RuntimeException {
    public RateProviderException(String message, Exception e) {
        super(message, e);
    }

    public RateProviderException(String message) {
        super(message);
    }

    public RateProviderException(Exception e) {
        super(e);
    }
}



