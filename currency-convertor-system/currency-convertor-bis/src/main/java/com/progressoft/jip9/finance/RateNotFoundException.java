package com.progressoft.jip9.finance;

public class RateNotFoundException extends RateProviderException {
    public RateNotFoundException(String message) {
        super(message);
    }
}
