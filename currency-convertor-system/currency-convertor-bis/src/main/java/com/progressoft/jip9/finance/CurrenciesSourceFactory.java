package com.progressoft.jip9.finance;

public interface CurrenciesSourceFactory {
     CurrencyValidator getCurrencyValidator();
     RateSupplier getRateSupplier();
}
