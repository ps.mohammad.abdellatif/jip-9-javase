package com.progressoft.jip9.finance.reader.impl;

import com.progressoft.jip9.finance.reader.Quarter;

import java.util.List;

class DefaultRatesQuarterly extends RatesBase<Quarter> implements QuarterlyRates {

    public DefaultRatesQuarterly(String[] record, Border quarterly, List<String> header) {
        super(record, quarterly, header);
    }

    @Override
    protected Quarter getKey(String header) {
        String[] split = header.split("-");
        int year = Integer.parseInt(split[0]);
        int quarter = Integer.parseInt(split[1].charAt(1) + "");
        return new Quarter(year, quarter);
    }


}