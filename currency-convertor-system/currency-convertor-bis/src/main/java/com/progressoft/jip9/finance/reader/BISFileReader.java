package com.progressoft.jip9.finance.reader;

import java.nio.file.Path;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Consumer;

public interface BISFileReader {

    void read(Path path, Consumer<RateRecord> consumer);

    default List<RateRecord> read(Path path) {
        List<RateRecord> records = new LinkedList<>();
        read(path, records::add);
        return records;
    }
}
