package com.progressoft.jip9.finance.reader;

public interface RateRecord {

    Frequency getFrequency();

    String getRefAreaCode();

    String getRefArea();

    String getCurrencyCode();

    String getCurrency();

    RateCollection getCollection();

    Rates getRates();
}
