package com.progressoft.jip9.finance.reader;

import java.time.YearMonth;

public interface MonthlyRates extends Rates<YearMonth> {
}
