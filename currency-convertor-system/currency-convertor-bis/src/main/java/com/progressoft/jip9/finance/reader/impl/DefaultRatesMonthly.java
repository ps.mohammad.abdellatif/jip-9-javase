package com.progressoft.jip9.finance.reader.impl;

import com.progressoft.jip9.finance.reader.MonthlyRates;

import java.time.YearMonth;
import java.util.List;

class DefaultRatesMonthly extends RatesBase<YearMonth> implements MonthlyRates {



    public DefaultRatesMonthly(String[] record, Border monthly, List<String> header) {
        super(record, monthly, header);
    }

    @Override
    protected YearMonth getKey(String header) {
        String[] split = header.split("-");
        return YearMonth.of(Integer.parseInt(split[0]), Integer.parseInt(split[1]));
    }


}