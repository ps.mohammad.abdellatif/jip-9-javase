package com.progressoft.jip9.finance;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Map;

public class BISRateSupplier implements RateSupplier {
    private final CSVStore csvStore;

    public BISRateSupplier(CSVStore csvStore) {
        this.csvStore = csvStore;
    }

    @Override
    public BigDecimal getRate(String from, String to) {
        isCodeExiste(from);
        isCodeExiste(to);
        BigDecimal fromRate = csvStore.getRateFromBase(from);
        BigDecimal toRate = csvStore.getRateFromBase(to);
        return toRate.divide(fromRate, 4, RoundingMode.HALF_UP);
    }

    private void isCodeExiste(String code) {
        if (!csvStore.currencyExists(code))
            throw new RateNotFoundException("no such a rate for: " + code);
    }

}
