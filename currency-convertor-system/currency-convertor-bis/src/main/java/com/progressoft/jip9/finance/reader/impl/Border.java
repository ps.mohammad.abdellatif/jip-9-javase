package com.progressoft.jip9.finance.reader.impl;

class Border {
    private final int first;
    private final int last;

    public Border(int first, int last) {
        this.first = first;
        this.last = last;
    }

    public int getFirst() {
        return first;
    }

    public int getLast() {
        return last;
    }
}
