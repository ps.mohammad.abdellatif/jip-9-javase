package com.progressoft.jip9.finance.reader.impl;

import com.progressoft.jip9.finance.reader.Quarter;
import com.progressoft.jip9.finance.reader.Rates;

public interface QuarterlyRates extends Rates<Quarter> {
}
