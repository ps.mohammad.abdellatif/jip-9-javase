package com.progressoft.jip9.finance;


public class BISCurrenciesValidator implements CurrencyValidator {
    private final CSVStore csvStore;

    public BISCurrenciesValidator(CSVStore csvStore) {
        this.csvStore = csvStore;
    }

    @Override
    public boolean isValid(String currency) {
        if (currency == null)
            throw new NullPointerException("Null currency");
        return csvStore.currencyExists(currency);
    }
}
