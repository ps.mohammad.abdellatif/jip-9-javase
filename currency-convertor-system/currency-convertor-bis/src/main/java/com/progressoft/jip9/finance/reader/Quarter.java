package com.progressoft.jip9.finance.reader;

import java.util.Objects;

public class Quarter {
    private final int year;
    private final int quarter;

    public Quarter(int year, int quarter) {
        this.year = year;
        this.quarter = quarter;
    }

    public static Quarter of(int year, int quarter) {
        return new Quarter(year, quarter);
    }

    public int getYear() {
        return year;
    }

    public int getQuarter() {
        return quarter;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Quarter quarter = (Quarter) o;
        return year == quarter.year &&
                this.quarter == quarter.quarter;
    }

    @Override
    public int hashCode() {
        return Objects.hash(year, quarter);
    }
}
