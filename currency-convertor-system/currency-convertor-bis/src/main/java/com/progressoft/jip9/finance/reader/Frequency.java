package com.progressoft.jip9.finance.reader;

public enum Frequency {
    ANNUAL, MONTHLY, QUARTERLY;
}
