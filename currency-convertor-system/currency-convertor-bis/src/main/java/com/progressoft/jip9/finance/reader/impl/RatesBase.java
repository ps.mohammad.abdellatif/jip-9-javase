package com.progressoft.jip9.finance.reader.impl;

import com.progressoft.jip9.finance.reader.Rates;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class RatesBase<K> implements Rates<K> {

    private K first;
    private K last;
    private Map<K, BigDecimal> rates = new HashMap<>();

    public RatesBase(String[] record, Border annual, List<String> header) {
        boolean setFirst = false;
        for (int i = annual.getFirst(); i <= annual.getLast(); i++) {
            if (record[i].equals("\"\""))
                continue;
            if (!setFirst) {
                first = getKey(getFieldValue(header.get(i)));
                setFirst = true;
            }
            last = getKey(getFieldValue(header.get(i)));
            rates.put(getKey(getFieldValue(header.get(i))), new BigDecimal(getFieldValue(record[i])));
        }
    }

    protected abstract K getKey(String header);

    private String getFieldValue(String value) {
        return value.substring(1, value.length() - 1);
    }

    @Override
    public K getFirst() {
        return first;
    }

    @Override
    public K getLast() {
        return last;
    }

    @Override
    public BigDecimal getRate(K yearMonth) {
        return rates.get(yearMonth);
    }
}
