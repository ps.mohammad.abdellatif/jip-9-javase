package com.progressoft.jip9.finance.reader.impl;

import com.progressoft.jip9.finance.reader.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.function.Consumer;

public class DefaultBISFileReader implements BISFileReader {
    // stateless service (object)
    private static final String[] HEADERS_TYPE_REGEX = {"\\d{4}", "\\d{4}-\\d{1,2}", "\\d{4}-Q\\d{1}"};

    @Override
    public void read(Path path, Consumer<RateRecord> consumer) {
        validatePath(path);
        try (BufferedReader reader = Files.newBufferedReader(path)) {
            readRecords(reader, (consumer));
        } catch (IOException e) {
            // TODO need to add a unit test to verify this catch
            throw new IllegalStateException(e);
        }
    }

    private void readRecords(BufferedReader reader, Consumer<? super RateRecord> consumer) throws IOException {
        String line = reader.readLine();
        if (line == null)
            return;
        FileStructure fileStructure = storeHeader(line);
        while ((line = reader.readLine()) != null) {
            RateRecord rateRecord = readLine(line, fileStructure);
            consumer.accept(rateRecord);
        }
    }

    private RateRecord readLine(String line, FileStructure fileStructure) {
        // TODO test case for invalid lines
        String[] fields = line.split(",", -1);
        Frequency frequency = Frequency.valueOf(getFieldValue(fields[1]).toUpperCase());
        RateCollection collection = RateCollection.byCode(getFieldValue(fields[6]));
        Rates rates = returnRates(fields, frequency, fileStructure);
        return new DefaultRateRecord()
                .setRefAreaCode(getFieldValue(fields[2]))
                .setRefArea(getFieldValue(fields[3]))
                .setCurrencyCode(getFieldValue(fields[4]))
                .setCurrency(getFieldValue(fields[5]))
                .setFrequency(frequency)
                .setCollection(collection)
                .setRates(rates);
    }

    private Rates returnRates(String[] fields, Frequency frequency, FileStructure fileStructure) {
        if (frequency == Frequency.ANNUAL)
            return new DefaultRatesAnnual(fields, fileStructure.annual, fileStructure.headers);
        if (frequency == Frequency.MONTHLY)
            return new DefaultRatesMonthly(fields, fileStructure.monthly, fileStructure.headers);
        return new DefaultRatesQuarterly(fields, fileStructure.quarterly, fileStructure.headers);
    }

    private String getFieldValue(String value) {
        return value.substring(1, value.length() - 1);
    }

    private void validatePath(Path path) {
        if (path == null)
            throw new NullPointerException("null path");
        if (Files.notExists(path))
            throw new IllegalArgumentException("path does not exists");
        if (Files.isDirectory(path))
            throw new IllegalArgumentException("path is not a file");
    }

    private FileStructure storeHeader(String line) {
        List<String> headers = Arrays.asList(line.split(",", -1));
        Border[] borders = findBorders(headers);

        return getFileStructure(headers, borders);
    }

    private Border[] findBorders(List<String> headers) {
        Border[] borders = new Border[HEADERS_TYPE_REGEX.length];
        int start = -1;
        int end = -1;
        int index = 0;
        for (int i = 9; i < headers.size(); ) {
            String header = getFieldValue(headers.get(i));
            if (!header.matches(HEADERS_TYPE_REGEX[index])) {
                borders[index++] = new Border(start, end);
                start = end = -1;
                continue;
            }
            if (start == -1)
                start = i;
            end = i;
            i++;
        }
        borders[index] = new Border(start, end);
        return borders;
    }

    private FileStructure getFileStructure(List<String> headers, Border[] borders) {
        FileStructure fileStructure = new FileStructure();
        fileStructure.headers = headers;
        fileStructure.annual = borders[0];
        fileStructure.monthly = borders[1];
        fileStructure.quarterly = borders[2];
        return fileStructure;
    }

    private class FileStructure {
        List<String> headers;
        Border annual;
        Border monthly;
        Border quarterly;
    }

    private static class DefaultRateRecord implements RateRecord {
        private Frequency frequency;
        private String refAreaCode;
        private String refArea;
        private String currencyCode;
        private String currency;
        private RateCollection collection;
        private Rates rates;

        @Override
        public Frequency getFrequency() {
            return frequency;
        }

        public DefaultRateRecord setFrequency(Frequency frequency) {
            this.frequency = frequency;
            return this;
        }

        @Override
        public String getRefAreaCode() {
            return refAreaCode;
        }

        public DefaultRateRecord setRefAreaCode(String refAreaCode) {
            this.refAreaCode = refAreaCode;
            return this;
        }

        @Override
        public String getRefArea() {
            return refArea;
        }

        public DefaultRateRecord setRefArea(String refArea) {
            this.refArea = refArea;
            return this;
        }

        @Override
        public String getCurrencyCode() {
            return currencyCode;
        }

        public DefaultRateRecord setCurrencyCode(String currencyCode) {
            this.currencyCode = currencyCode;
            return this;
        }

        @Override
        public String getCurrency() {
            return currency;
        }

        public DefaultRateRecord setCurrency(String currency) {
            this.currency = currency;
            return this;
        }

        @Override
        public RateCollection getCollection() {
            return collection;
        }

        public DefaultRateRecord setCollection(RateCollection collection) {
            this.collection = collection;
            return this;
        }


        @Override
        public Rates getRates() {
            return rates;
        }

        public DefaultRateRecord setRates(Rates rates) {
            this.rates = rates;
            return this;
        }
    }
}
