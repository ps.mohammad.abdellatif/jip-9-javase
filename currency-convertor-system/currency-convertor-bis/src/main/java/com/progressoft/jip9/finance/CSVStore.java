package com.progressoft.jip9.finance;

import java.io.*;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collectors;


public class CSVStore {

    public static final int CURRENCY_INDEX = 2;

    private Path path;
    private Map<String, BigDecimal> ratesMap = new HashMap<>();

    public CSVStore(Path path) throws IOException {
        this.path = path;
        isValidPath(path);
        readAndStore();
    }

    public boolean currencyExists(String currency) {
        return ratesMap.containsKey(currency);
    }

    public BigDecimal getRateFromBase(String currency) {
        return ratesMap.get(currency);
    }

    public void readAndStore() {
        try (BufferedReader reader = Files.newBufferedReader(path)) {
            String line;
            while ((line = reader.readLine()) != null) {
                String[] text = line.split(",");
                String currency = extractCurrencyCode(text[CURRENCY_INDEX]);
                // TODO we need to fix this
                String lastReadRatio = text[text.length - 1];
                ratesMap.put(currency, new BigDecimal(lastReadRatio));
            }
        } catch (IOException e) {
            throw new RateProviderException(e);
        }
    }

    public List<String> getCurrencies(){
        return new ArrayList<>(ratesMap.keySet());
    }

    private String extractCurrencyCode(String s) {
        return s.substring(1, 4);
    }

    private void isValidPath(Path path) throws FileNotFoundException {
        failIfNull(path);
        failIfDirectory(path);
        failIfNotExists(path);
    }

    private void failIfDirectory(Path path) {
        if (Files.isDirectory(path))
            throw new IllegalArgumentException("Error, there is no such a file in this path: " + path);
    }

    private void failIfNotExists(Path path) throws FileNotFoundException {
        if (Files.notExists(path))
            throw new FileNotFoundException("Error, The file dose not exists");
    }

    private void failIfNull(Path path) {
        if (path == null)
            throw new NullPointerException("Error, the path is null!");
    }
}
