package com.progressoft.jip9.finance.reader;

import java.math.BigDecimal;

public interface Rates<K> {

    K getFirst();

    K getLast();

    BigDecimal getRate(K key);
}
