package com.progressoft.jip9.finance;

public class BISCurrenciesSourceFactory implements CurrenciesSourceFactory {

    private BISCurrenciesValidator currenciesValidator;
    private BISRateSupplier rateSupplier;
    public BISCurrenciesSourceFactory(CSVStore csvStore){
        rateSupplier = new BISRateSupplier(csvStore);
        currenciesValidator = new BISCurrenciesValidator(csvStore);
    }
    @Override
    public CurrencyValidator getCurrencyValidator() {
        return currenciesValidator;
    }
    @Override
    public RateSupplier getRateSupplier() {
        return rateSupplier;
    }
}
