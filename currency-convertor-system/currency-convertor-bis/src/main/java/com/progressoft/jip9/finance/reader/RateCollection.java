package com.progressoft.jip9.finance.reader;

public enum RateCollection {
    AVERAGE("A"), END_OF_PERIOD("E");

    private final String shortName;

    RateCollection(String shortName) {
        this.shortName = shortName;
    }

    public String getShortName() {
        return shortName;
    }

    public static RateCollection byCode(String code) {
        switch (code) {
            case "A":
                return AVERAGE;
            case "E":
                return END_OF_PERIOD;
        }
        throw new IllegalArgumentException("Unknown code: " + code);
    }
}
