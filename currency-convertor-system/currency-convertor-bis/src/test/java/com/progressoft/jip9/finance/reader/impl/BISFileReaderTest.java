package com.progressoft.jip9.finance.reader.impl;

import com.progressoft.jip9.finance.reader.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.YearMonth;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class BISFileReaderTest {

    private BISFileReader reader;

    @BeforeEach
    public void setup() {
        reader = new DefaultBISFileReader();
    }

    @Test
    public void canCreate() {
        new DefaultBISFileReader();
    }

    @Test
    public void givenNullPathToFile_whenRead_thenFail() {
        Path path = null;
        NullPointerException thrown = Assertions.assertThrows(NullPointerException.class,
                () -> reader.read(path));
        Assertions.assertEquals("null path", thrown.getMessage());
    }

    @Test
    public void givenNotExistsPath_whenRead_thenFail() {
        Path path = Paths.get(".", "dir" + new Random().nextInt());
        IllegalArgumentException thrown = Assertions.assertThrows(IllegalArgumentException.class,
                () -> reader.read(path));
        Assertions.assertEquals("path does not exists", thrown.getMessage());
    }

    @Test
    public void givenValidDirectoryPath_whenRead_thenFail() throws IOException {
        Path path = Files.createTempDirectory("bis-dir");
        IllegalArgumentException thrown = Assertions.assertThrows(IllegalArgumentException.class,
                () -> reader.read(path));
        Assertions.assertEquals("path is not a file", thrown.getMessage());
    }

    @Test
    public void givenValidPath_whenRead_thenSuccess() throws IOException {
        Path path = Files.createTempFile("bis", ".csv");
        try (InputStream resourceAsStream = this.getClass().getResourceAsStream("/bis-full-data.csv");
             OutputStream outputStream = Files.newOutputStream(path)) {
            int read;
            while ((read = resourceAsStream.read()) != -1)
                outputStream.write(read);
        }

        List<RateRecord> records = new ArrayList<>();

        reader.read(path, records::add);

        Assertions.assertNotNull(records);
        Assertions.assertEquals(records.size(), 6, "expected 6 records");

        RateRecord rateRecord = records.get(0);
        Assertions.assertNotNull(rateRecord);
        Assertions.assertEquals(Frequency.ANNUAL, rateRecord.getFrequency());
        Assertions.assertEquals("AE", rateRecord.getRefAreaCode());
        Assertions.assertEquals("United Arab Emirates", rateRecord.getRefArea());
        Assertions.assertEquals("AED", rateRecord.getCurrencyCode());
        Assertions.assertEquals("UAE Dirham", rateRecord.getCurrency());
        Assertions.assertEquals(RateCollection.AVERAGE, rateRecord.getCollection());

        Rates rates = rateRecord.getRates();
        Assertions.assertNotNull(rates);
        Assertions.assertTrue(rates instanceof YearlyRates);
        YearlyRates yearlyRates = (YearlyRates) rates;
        Assertions.assertEquals(yearlyRates.getFirst(), 1966);
        Assertions.assertEquals(yearlyRates.getLast(), 2019);
        Assertions.assertEquals(new BigDecimal("4.7619"), yearlyRates.getRate(1970));

        rateRecord = records.get(1);
        Assertions.assertNotNull(rateRecord);
        Assertions.assertEquals(Frequency.MONTHLY, rateRecord.getFrequency());
        Assertions.assertEquals("AE", rateRecord.getRefAreaCode());
        Assertions.assertEquals("United Arab Emirates", rateRecord.getRefArea());
        Assertions.assertEquals("AED", rateRecord.getCurrencyCode());
        Assertions.assertEquals("UAE Dirham", rateRecord.getCurrency());
        Assertions.assertEquals(RateCollection.AVERAGE, rateRecord.getCollection());

        rates = rateRecord.getRates();
        Assertions.assertNotNull(rates);
        Assertions.assertTrue(rates instanceof MonthlyRates);
        MonthlyRates monthlyRates = (MonthlyRates) rates;
        Assertions.assertEquals(YearMonth.of(1966, 1), monthlyRates.getFirst());
        Assertions.assertEquals(YearMonth.of(2020, 5), monthlyRates.getLast());
        Assertions.assertEquals(new BigDecimal("4.7619"), monthlyRates.getRate(YearMonth.of(1971, 10)));


        rateRecord = records.get(2);
        Assertions.assertNotNull(rateRecord);
        Assertions.assertEquals(Frequency.QUARTERLY, rateRecord.getFrequency());
        Assertions.assertEquals("AE", rateRecord.getRefAreaCode());
        Assertions.assertEquals("United Arab Emirates", rateRecord.getRefArea());
        Assertions.assertEquals("AED", rateRecord.getCurrencyCode());
        Assertions.assertEquals("UAE Dirham", rateRecord.getCurrency());
        Assertions.assertEquals(RateCollection.AVERAGE, rateRecord.getCollection());

        rates = rateRecord.getRates();
        Assertions.assertNotNull(rates);
        Assertions.assertTrue(rates instanceof QuarterlyRates);
        QuarterlyRates quarterlyRates = (QuarterlyRates) rates;
        Assertions.assertEquals(Quarter.of(1966, 1), quarterlyRates.getFirst());
        Assertions.assertEquals(Quarter.of(2020, 1), quarterlyRates.getLast());
        Assertions.assertEquals(new BigDecimal("4.7619"), quarterlyRates.getRate(Quarter.of(1971, 1)));


        rateRecord = records.get(3);
        Assertions.assertNotNull(rateRecord);
        Assertions.assertEquals(Frequency.ANNUAL, rateRecord.getFrequency());
        Assertions.assertEquals("AE", rateRecord.getRefAreaCode());
        Assertions.assertEquals("United Arab Emirates", rateRecord.getRefArea());
        Assertions.assertEquals("AED", rateRecord.getCurrencyCode());
        Assertions.assertEquals("UAE Dirham", rateRecord.getCurrency());
        Assertions.assertEquals(RateCollection.END_OF_PERIOD, rateRecord.getCollection());

        rates = rateRecord.getRates();
        Assertions.assertNotNull(rates);
        Assertions.assertTrue(rates instanceof YearlyRates);
        yearlyRates = (YearlyRates) rates;
        Assertions.assertEquals(yearlyRates.getFirst(), 1966);
        Assertions.assertEquals(yearlyRates.getLast(), 2019);
        Assertions.assertEquals(new BigDecimal("4.7619"), yearlyRates.getRate(1970));


        rateRecord = records.get(4);
        Assertions.assertNotNull(rateRecord);
        Assertions.assertEquals(Frequency.MONTHLY, rateRecord.getFrequency());
        Assertions.assertEquals("AE", rateRecord.getRefAreaCode());
        Assertions.assertEquals("United Arab Emirates", rateRecord.getRefArea());
        Assertions.assertEquals("AED", rateRecord.getCurrencyCode());
        Assertions.assertEquals("UAE Dirham", rateRecord.getCurrency());
        Assertions.assertEquals(RateCollection.END_OF_PERIOD, rateRecord.getCollection());

        rates = rateRecord.getRates();
        Assertions.assertNotNull(rates);
        Assertions.assertTrue(rates instanceof MonthlyRates);
        monthlyRates = (MonthlyRates) rates;
        Assertions.assertEquals(YearMonth.of(1966, 1), monthlyRates.getFirst());
        Assertions.assertEquals(YearMonth.of(2020, 5), monthlyRates.getLast());
        Assertions.assertEquals(new BigDecimal("4.7619"), monthlyRates.getRate(YearMonth.of(1971, 10)));


        rateRecord = records.get(5);
        Assertions.assertNotNull(rateRecord);
        Assertions.assertEquals(Frequency.QUARTERLY, rateRecord.getFrequency());
        Assertions.assertEquals("AE", rateRecord.getRefAreaCode());
        Assertions.assertEquals("United Arab Emirates", rateRecord.getRefArea());
        Assertions.assertEquals("AED", rateRecord.getCurrencyCode());
        Assertions.assertEquals("UAE Dirham", rateRecord.getCurrency());
        Assertions.assertEquals(RateCollection.END_OF_PERIOD, rateRecord.getCollection());

        rates = rateRecord.getRates();
        Assertions.assertNotNull(rates);
        Assertions.assertTrue(rates instanceof QuarterlyRates);
        quarterlyRates = (QuarterlyRates) rates;
        Assertions.assertEquals(Quarter.of(1966, 1), quarterlyRates.getFirst());
        Assertions.assertEquals(Quarter.of(2020, 1), quarterlyRates.getLast());
        Assertions.assertEquals(new BigDecimal("4.7619"), quarterlyRates.getRate(Quarter.of(1971, 1)));
    }
}
