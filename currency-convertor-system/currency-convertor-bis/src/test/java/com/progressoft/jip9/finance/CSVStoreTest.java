package com.progressoft.jip9.finance;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


public class CSVStoreTest {

    static Path path = Paths.get(".", "dir1");
    static Path textFilePath = path.resolve("WEBSTATS_XRU_CURRENT_DATAFLOW_csv_col.csv");


    @Test
    public void givenNullPath_whenCSVStore_thenThrowException(){
        NullPointerException exception = assertThrows(NullPointerException.class, () -> new CSVStore(null));
        assertEquals("Error, the path is null!", exception.getMessage());
    }
    @Test
    public void givenInvalidFileName_whenCSVStore_thenThrowException() {
        Path InvalidFileName = Paths.get(".", "Amani.csv");
        assertThrows(FileNotFoundException.class, () -> new CSVStore(InvalidFileName));
        FileNotFoundException exception = assertThrows(FileNotFoundException.class, () -> new CSVStore(InvalidFileName));
        assertEquals("Error, The file dose not exists", exception.getMessage());
    }
    @Test
    public void givenInvalidDirectory_whenCSVStore_thenThrowException() {
        Path directory = Paths.get(".");
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> new CSVStore(directory));
        assertEquals("Error, there is no such a file in this path: "+ directory, exception.getMessage());
    }

    @Test
    public void givenValidPath_whenCSVStore_thenStoreDataCorrectly() throws IOException {
        CSVStore CSVStoreData = new CSVStore(textFilePath);
        // TODO add success scenarios
    }

}
