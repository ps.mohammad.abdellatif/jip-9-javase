package com.progressoft.jip9.finance;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class BISCurrenciesValidatorTest {



    static Path path = Paths.get(".", "dir1");
    static Path textFilePath = path.resolve("WEBSTATS_XRU_CURRENT_DATAFLOW_csv_col.csv");


    @Test
    public void givenNullPath_whenValidating_thenThrowException(){
        NullPointerException exception = assertThrows(NullPointerException.class, () -> new BISCurrenciesValidator(new CSVStore(null)));
        assertEquals("Error, the path is null!", exception.getMessage());
    }
    @Test
    public void givenInvalidFileName_whenValidating_thenThrowException() {
        Path InvalidFileName = Paths.get(".", "Amani.csv");
        assertThrows(FileNotFoundException.class, () -> new CSVStore(InvalidFileName));
        FileNotFoundException exception = assertThrows(FileNotFoundException.class, () -> new BISCurrenciesValidator(new CSVStore(InvalidFileName)));
        assertEquals("Error, The file dose not exists", exception.getMessage());
    }
    @Test
    public void givenInvalidDirectory_whenValidating_thenThrowException() {
        Path directory = Paths.get(".");
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> new BISCurrenciesValidator(new CSVStore(directory)));
        assertEquals("Error, there is no such a file in this path: "+ directory, exception.getMessage());
    }

    @Test
    public void givenNullCurrency_whenValidating_thenThrowNullPointerException() throws IOException {
        CurrencyValidator validator = new BISCurrenciesValidator(new CSVStore(textFilePath));
        NullPointerException nullPointerException = Assertions.assertThrows(NullPointerException.class, () -> validator.isValid(null));
        Assertions.assertEquals("Null currency",nullPointerException.getMessage());
    }
    @Test
    public void givenCurrency_whenIsValid_thenReturnExpectedResult() throws IOException {
        CurrencyValidator validator = new BISCurrenciesValidator(new CSVStore(textFilePath));
        Assertions.assertFalse(validator.isValid("DJO"));
        Assertions.assertTrue(validator.isValid("OMR"));
        Assertions.assertFalse(validator.isValid("AAA"));
        Assertions.assertTrue(validator.isValid("USD"));
        Assertions.assertFalse(validator.isValid("A2A"));
        Assertions.assertTrue(validator.isValid("JOD"));
    }


    /*
   static Path path;
   @BeforeAll
    public static void copyAndCreateNewPath() throws IOException {
        try (InputStream is = BISRateSupplierTest.class.getResourceAsStream("/WEBSTATS_XRU_CURRENT_DATAFLOW_csv_col.csv")) {
            path = Files.createTempFile("temp", ".csv");
            try (OutputStream outputStream = Files.newOutputStream(path)) {
                int length;
                byte[] bytes = new byte[1024 * 8];
                while ((length = is.read(bytes)) > 0) {
                    outputStream.write(bytes, 0, length);
                }
                outputStream.flush();
            }
        }
    }*/


}
