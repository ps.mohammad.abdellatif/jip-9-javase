package com.progressoft.jip9.finance;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;

public class MainBISCurrenciesSourceFactory {

    public static void main(String[] args) throws IOException {

        Path path = Paths.get(".", "dir1");
        Path textFilePath = path.resolve("WEBSTATS_XRU_CURRENT_DATAFLOW_csv_col.csv");


        CurrenciesSourceFactory bisType = new BISCurrenciesSourceFactory(new CSVStore(textFilePath));

        CurrencyValidator validator = bisType.getCurrencyValidator();
        RateSupplier bisRateSupplier = bisType.getRateSupplier();

        System.out.println(validator.isValid("ASD"));
        System.out.println(validator.isValid("TRS"));
        System.out.println(validator.isValid("OMR"));
        System.out.println(bisRateSupplier.getRate("OMR", "JOD"));

    }
}