package com.progressoft.jip9.finance;

import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class BISRateSupplierTest {

    static Path path = Paths.get(".", "dir1");
    static Path textFilePath  = path.resolve("WEBSTATS_XRU_CURRENT_DATAFLOW_csv_col.csv");

    @Test
    public void givenNullPath_whenGetRate_thenThrowException(){
        NullPointerException exception = assertThrows(NullPointerException.class,  () -> new BISRateSupplier(new CSVStore(null)));
        assertEquals("Error, the path is null!", exception.getMessage());
    }
    @Test
    public void givenInvalidFileName_whenGetRate_thenThrowException() {
        Path InvalidFileName = Paths.get(".", "Amani.csv");
        assertThrows(FileNotFoundException.class, () -> new CSVStore(InvalidFileName));
        FileNotFoundException exception = assertThrows(FileNotFoundException.class, () -> new BISRateSupplier(new CSVStore(InvalidFileName)));
        assertEquals("Error, The file dose not exists", exception.getMessage());
    }
    @Test
    public void givenInvalidDirectory_whenGetRate_thenThrowException() {
        Path directory = Paths.get(".");
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> new BISRateSupplier(new CSVStore(directory)));
        assertEquals("Error, there is no such a file in this path: "+ directory, exception.getMessage());
    }

    @Test
    public void givenUnKnownCurrencies_whenGetRate_thenThrowException() throws IOException {
        RateSupplier bisRateSupplier = new BISRateSupplier(new CSVStore(textFilePath));
        RateNotFoundException exception = assertThrows(RateNotFoundException.class, () -> bisRateSupplier.getRate("AAA","USD"));
        assertEquals("no such a rate for: AAA", exception.getMessage());
    }


    @Test
    public void givenFromUSDToAnotherCurrency_whenGetRate_thenReturnResult() throws IOException {
        RateSupplier bisRateSupplier = new BISRateSupplier(new CSVStore(textFilePath));

        BigDecimal rate = bisRateSupplier.getRate("USD", "USD");
        assertEquals(BigDecimal.valueOf(1).setScale(4), rate);
        //To OMR
         rate = bisRateSupplier.getRate("USD", "OMR");
        assertEquals(BigDecimal.valueOf(0.3845).setScale(4), rate);
       // To JOD
        rate = bisRateSupplier.getRate("USD", "JOD");
        assertEquals(BigDecimal.valueOf(0.71).setScale(4), rate);

    }

    @Test
    public void givenFromOthCurrencyToUSD_whenGetRate_thenReturnResult() throws IOException {
        RateSupplier bisRateSupplier = new BISRateSupplier(new CSVStore(textFilePath));
        BigDecimal rate = bisRateSupplier.getRate("JOD", "USD");
        assertEquals(BigDecimal.valueOf(1.4085), rate);
    }

    @Test
    public void givenFromOMRToJOD_whenGetRate_thenReturnResult() throws IOException {
        RateSupplier bisRateSupplier = new BISRateSupplier(new CSVStore(textFilePath));
        BigDecimal rate = bisRateSupplier.getRate("OMR", "JOD");
        assertEquals(BigDecimal.valueOf(1.8466).setScale(4), rate);
    }

}
