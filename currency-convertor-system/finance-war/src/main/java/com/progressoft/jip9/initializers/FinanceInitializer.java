package com.progressoft.jip9.initializers;

import com.progressoft.jip9.servlets.ServerInfoServlet;

import javax.servlet.ServletContainerInitializer;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;
import java.util.Set;

public class FinanceInitializer implements ServletContainerInitializer {

    @Override
    public void onStartup(Set<Class<?>> plugins, ServletContext ctx) throws ServletException {
        ServerInfoServlet serverInfoServlet = new ServerInfoServlet("finance app");
        ServletRegistration.Dynamic servletRegistration = ctx.addServlet("serverInfoServlet", serverInfoServlet);
        servletRegistration.addMapping("/server-info","/info");
        servletRegistration.setLoadOnStartup(-1);
    }
}
