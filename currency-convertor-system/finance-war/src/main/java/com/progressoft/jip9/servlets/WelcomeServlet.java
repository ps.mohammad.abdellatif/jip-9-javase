package com.progressoft.jip9.servlets;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;

public class WelcomeServlet extends HttpServlet {

    @Override
    public void init() throws ServletException {
        System.out.println("welcome==>initialized");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        resp.setContentType("text/plain");
        resp.setStatus(HttpServletResponse.SC_ACCEPTED);
        PrintWriter writer = resp.getWriter();
        writer.println("Welcome to our sample");
        writer.println("your requested this page at: " + LocalDateTime.now());
        writer.flush();
    }

    @Override
    public void destroy() {
        System.out.println("welcome==>destroyed");
    }
}
