package com.progressoft.jip9.servlets;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class ServerInfoServlet extends HttpServlet {

    private final String applicationName;

    public ServerInfoServlet(String applicationName) {
        this.applicationName = applicationName;
    }

    @Override
    public void init() throws ServletException {
        System.out.println("info==>initialized");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        PrintWriter writer = resp.getWriter();
        writer.println("<html><body>");
        writer.append(applicationName).append(" Server Info");
        writer.println("</body></html>");
    }

    @Override
    public void destroy() {
        System.out.println("info==>destroyed");
    }
}
