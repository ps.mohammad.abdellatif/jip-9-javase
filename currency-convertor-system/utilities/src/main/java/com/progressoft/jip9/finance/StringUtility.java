package com.progressoft.jip9.finance;

public class StringUtility {

    public static String toUpper(String value) {
        if (value == null)
            return null;
        return value.toUpperCase();
    }
}
