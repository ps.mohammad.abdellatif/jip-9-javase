package com.progressoft.jip9.finance;

import com.progressoft.jip9.finance.reader.RateRecord;
import com.progressoft.jip9.finance.reader.YearlyRates;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class BISImporter {
    public static final String INSERT_YEARLY_RATES_SQL = "insert into bis_yearly_rates (ref_cd, year, rate, rate_colc) values (?,?,?,?)";
    public static final String INSERT_INTO_AREAS_SQL = "insert into bis_areas (ref_cd, ref_name,curr_cd) values (?,?,?)";
    public static final String INSERT_INTO_CURRENCIES_SQL = "insert into bis_currencies (curr_cd,curr_name) values (?,?)";
    private final DataSource dataSource;

    public BISImporter(DataSource dataSource) {
        this.dataSource = dataSource;
        if (dataSource == null)
            throw new NullPointerException("null datasource");
    }

    public void importRecord(RateRecord record) {
        if (record == null)
            throw new NullPointerException("null record");

        try (Connection connection = dataSource.getConnection()) {
            insertIntoCurrencies(record, connection);
            insertIntoAreas(record, connection);
            insertIntoYearlyRates(record, connection);
        } catch (SQLException e) {
            throw new ImportException(e);
        }
    }

    private void insertIntoYearlyRates(RateRecord record, Connection connection) throws SQLException {
        try (PreparedStatement preparedStatement = connection.prepareStatement(
                INSERT_YEARLY_RATES_SQL)) {
            YearlyRates rates = (YearlyRates) record.getRates();
            for (int year = rates.getFirst(); year <= rates.getLast(); year++) {
                preparedStatement.setString(1, record.getRefAreaCode());
                preparedStatement.setInt(2, year);
                preparedStatement.setBigDecimal(3, rates.getRate(year));
                preparedStatement.setString(4, record.getCollection().getShortName());
                preparedStatement.addBatch();
            }
            preparedStatement.executeBatch();
        }
    }

    private void insertIntoAreas(RateRecord record, Connection connection) throws SQLException {
        try (PreparedStatement preparedStatement = connection.prepareStatement(
                INSERT_INTO_AREAS_SQL)) {
            preparedStatement.setString(1, record.getRefAreaCode());
            preparedStatement.setString(2, record.getRefArea());
            preparedStatement.setString(3, record.getCurrencyCode());
            preparedStatement.executeUpdate();
        }
    }

    private void insertIntoCurrencies(RateRecord record, Connection connection) throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement(
                INSERT_INTO_CURRENCIES_SQL)) {
            statement.setString(1, record.getCurrencyCode());
            statement.setString(2, record.getCurrency());
            statement.executeUpdate();
        }
    }
}
