package com.progressoft.jip9.finance;

public class ImportException extends RuntimeException {
    public ImportException(Exception e) {
        super(e);
    }
}
