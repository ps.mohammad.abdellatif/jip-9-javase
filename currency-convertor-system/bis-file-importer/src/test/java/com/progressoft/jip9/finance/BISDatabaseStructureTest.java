package com.progressoft.jip9.finance;

import org.h2.jdbcx.JdbcDataSource;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.sql.DataSource;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.*;

public class BISDatabaseStructureTest {

    @Test
    public void canCreate() {
        new BISDatabaseInitializer();
    }

    @Test
    public void givenNullDataSource_whenInitialize_thenFail() {
        BISDatabaseInitializer initializer = new BISDatabaseInitializer();
        DataSource dataSource = null;
        NullPointerException thrown = Assertions.assertThrows(NullPointerException.class,
                () -> initializer.initialize(dataSource));
        Assertions.assertEquals("null datasource", thrown.getMessage());
    }

    @Test
    public void givenValidDatasource_whenInitialize_thenSuccess() throws IOException, SQLException {
        BISDatabaseInitializer initializer = new BISDatabaseInitializer();
        JdbcDataSource dataSource = new JdbcDataSource();
        Path bis_db = Files.createTempFile("bis_db", ".db");
        System.out.println(bis_db);
        dataSource.setUrl("jdbc:h2:file:" + bis_db);
        dataSource.setUser("sa");
        dataSource.setPassword("sa");

        initializer.initialize(dataSource);

        try (Connection connection = dataSource.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement("select * from bis_currencies")) {
                try(ResultSet resultSet = preparedStatement.executeQuery()){

                }
            }
            try (PreparedStatement preparedStatement = connection.prepareStatement("select * from bis_yearly_rates")) {
                try(ResultSet resultSet = preparedStatement.executeQuery()){

                }
            }
            try (PreparedStatement preparedStatement = connection.prepareStatement("select * from bis_areas")) {
                try(ResultSet resultSet = preparedStatement.executeQuery()){

                }
            }

        }
    }
}
