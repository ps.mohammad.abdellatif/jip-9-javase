package com.progressoft.jip9.finance;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class CalculatorInvocationHandler implements InvocationHandler {
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        String methodName = method.getName();
        if (methodName.equals("add")) {
            int f = (int) args[0];
            int s = (int) args[1];
            return f + s;
        }
        if (methodName.equals("subtract")) {
            int f = (int) args[0];
            int s = (int) args[1];
            return f - s;
        }
        throw new UnsupportedOperationException("unsupported method: " + methodName);
    }
}
