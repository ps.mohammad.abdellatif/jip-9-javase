package com.progressoft.jip9.finance;

import com.progressoft.jip9.finance.reader.*;
import org.h2.jdbcx.JdbcDataSource;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.sql.DataSource;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.*;
import java.util.Arrays;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Logger;

public class BISImporterTest {

    @Test
    public void giveNullDataSource_whenConstructImporter_thenFail() {
        DataSource dataSource = null;
        NullPointerException thrown = Assertions.assertThrows(NullPointerException.class, () -> new BISImporter(dataSource));
        Assertions.assertEquals("null datasource", thrown.getMessage());
    }

    @Test
    public void givenNullRecord_whenImport_thenFail() {
        // Path to file
        // DataSource: to connect to database
        DataSource dataSource = new JdbcDataSource();

        BISImporter importer = new BISImporter(dataSource);
        RateRecord record = null;

        NullPointerException thrown = Assertions.assertThrows(NullPointerException.class, () ->
                importer.importRecord(record));
        Assertions.assertEquals("null record", thrown.getMessage());
    }

    @Test
    public void givenNotNullEndOfPeriodAnnualRecord_whenImport_thenRecordImportedSuccessfully() throws SQLException, IOException {
        JdbcDataSource dataSource = new JdbcDataSource();
        Path bisDb = Files.createTempFile("bis_db", ".db");
        dataSource.setUrl("jdbc:h2:file:" + bisDb);
        dataSource.setUser("sa");
        dataSource.setPassword("sa");

        BISDatabaseInitializer initializer = new BISDatabaseInitializer();
        initializer.initialize(dataSource);
        AtomicInteger counter = new AtomicInteger(0);
        boolean[] closeFlag = new boolean[1];
        Connection[] connections = new Connection[1];
        DataSource proxyDataSource = (DataSource) Proxy
                .newProxyInstance(Thread.currentThread().getContextClassLoader(),
                        new Class[]{DataSource.class},
                        (proxy, method, args) -> {
                            failIfNotGetConnection(method, args);
                            Assertions.assertEquals(1, counter.incrementAndGet(), "getConnection was called more than once");
                            Connection connection = (Connection) method.invoke(dataSource, args);
                            connections[0] = connection;
                            return monitorConnection(closeFlag, connection);
                        });

        BISImporter importer = new BISImporter(proxyDataSource);

        RateRecord record = getEndOfPeriodAnnualRecord();

        importer.importRecord(record);

        Assertions.assertTrue(closeFlag[0], "close is never called");
        Assertions.assertTrue(connections[0].isClosed(), "close is never called");

        try (Connection connection = dataSource.getConnection()) {
            assertRefArea(connection);
            assertCurrency(connection);
            assertAnnualRates(connection);
        }
    }

    private Object monitorConnection(boolean[] closeFlag, Connection connection) {
        return Proxy.newProxyInstance(Thread.currentThread().getContextClassLoader(),
                connection.getClass().getInterfaces(),
                (proxy1, method1, args1) -> {
                    if (method1.getName().equals("close")) {
                        closeFlag[0] = true;
                    }
                    return method1.invoke(connection, args1);
                });
    }

    private void failIfNotGetConnection(Method method, Object[] args) {
        if (!method.getName().equals("getConnection")
                || (args != null && args.length != 0)) {
            Assertions.fail("calling unneeded method: " + method.getName() + " with args: " + Arrays.toString(args));
        }
    }

    private void assertAnnualRates(Connection connection) throws SQLException {
        try (PreparedStatement preparedStatement = connection.prepareStatement(
                "select ref_cd, year, rate, rate_colc from bis_yearly_rates")) {
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                Assertions.assertTrue(resultSet.next());
                Assertions.assertEquals("US", resultSet.getString("ref_cd"));
                Assertions.assertEquals(Integer.valueOf(2017), resultSet.getInt("year"));
                Assertions.assertEquals(0, new BigDecimal("0.99").compareTo(resultSet.getBigDecimal("rate")));
                Assertions.assertEquals("E", resultSet.getString("rate_colc"));

                Assertions.assertTrue(resultSet.next());
                Assertions.assertEquals("US", resultSet.getString("ref_cd"));
                Assertions.assertEquals(Integer.valueOf(2018), resultSet.getInt("year"));
                Assertions.assertEquals(0, new BigDecimal("0.95").compareTo(resultSet.getBigDecimal("rate")));
                Assertions.assertEquals("E", resultSet.getString("rate_colc"));

                Assertions.assertTrue(resultSet.next());
                Assertions.assertEquals("US", resultSet.getString("ref_cd"));
                Assertions.assertEquals(Integer.valueOf(2019), resultSet.getInt("year"));
                Assertions.assertEquals(0, new BigDecimal("0.8").compareTo(resultSet.getBigDecimal("rate")));
                Assertions.assertEquals("E", resultSet.getString("rate_colc"));
            }
        }
    }

    private void assertCurrency(Connection connection) throws SQLException {
        try (PreparedStatement currQuery = connection.prepareStatement("select curr_cd, curr_name from bis_currencies")) {
            try (ResultSet resultSet = currQuery.executeQuery()) {
                Assertions.assertTrue(resultSet.next(), "no records are returned");
                Assertions.assertEquals("USD", resultSet.getString("curr_cd"));
                Assertions.assertEquals("Dollar", resultSet.getString("curr_name"));
            }
        }
    }

    private void assertRefArea(Connection connection) throws SQLException {
        try (PreparedStatement areaQuery = connection.prepareStatement("select ref_cd, ref_name, curr_cd from bis_areas")) {
            try (ResultSet resultSet = areaQuery.executeQuery()) {
                Assertions.assertTrue(resultSet.next(), "no records are returned");
                Assertions.assertEquals("US", resultSet.getString("ref_cd"));
                Assertions.assertEquals("United States", resultSet.getString("ref_name"));
                Assertions.assertEquals("USD", resultSet.getString("curr_cd"));
                Assertions.assertFalse(resultSet.next(), "here is more records returned");
            }
        }
    }

    private RateRecord getEndOfPeriodAnnualRecord() {
        return new RateRecord() {
            @Override
            public Frequency getFrequency() {
                return Frequency.ANNUAL;
            }

            @Override
            public String getRefAreaCode() {
                return "US";
            }

            @Override
            public String getRefArea() {
                return "United States";
            }

            @Override
            public String getCurrencyCode() {
                return "USD";
            }

            @Override
            public String getCurrency() {
                return "Dollar";
            }

            @Override
            public RateCollection getCollection() {
                return RateCollection.END_OF_PERIOD;
            }

            @Override
            public Rates getRates() {
                return new YearlyRates() {
                    @Override
                    public Integer getFirst() {
                        return 2017;
                    }

                    @Override
                    public Integer getLast() {
                        return 2019;
                    }

                    @Override
                    public BigDecimal getRate(Integer year) {
                        switch (year) {
                            case 2017:
                                return new BigDecimal("0.99");
                            case 2018:
                                return new BigDecimal("0.95");
                            case 2019:
                                return new BigDecimal("0.8");
                        }
                        Assertions.fail("unknown year: " + year);
                        return null;
                    }
                };
            }
        };
    }

    private class MonitorDataSource implements DataSource {

        private DataSource dataSource;
        Connection connection;
        ConnectionMonitor connectionMonitor;
        int counter = 0;

        private MonitorDataSource(DataSource dataSource) {
            this.dataSource = dataSource;
        }


        @Override
        public Connection getConnection() throws SQLException {
            Assertions.assertEquals(0, counter++, "you are getting " + counter + " connections");

            connection = dataSource.getConnection();

            connectionMonitor = new ConnectionMonitor(connection);
            return connectionMonitor;
        }

        @Override
        public Connection getConnection(String username, String password) throws SQLException {
            Assertions.fail("your should not call this method");
            return dataSource.getConnection(username, password);
        }

        @Override
        public PrintWriter getLogWriter() throws SQLException {
            return dataSource.getLogWriter();
        }

        @Override
        public void setLogWriter(PrintWriter out) throws SQLException {
            dataSource.setLogWriter(out);
        }

        @Override
        public void setLoginTimeout(int seconds) throws SQLException {
            dataSource.setLoginTimeout(seconds);
        }

        @Override
        public int getLoginTimeout() throws SQLException {
            return dataSource.getLoginTimeout();
        }

        @Override
        public Logger getParentLogger() throws SQLFeatureNotSupportedException {
            return dataSource.getParentLogger();
        }

        @Override
        public <T> T unwrap(Class<T> iface) throws SQLException {
            return dataSource.unwrap(iface);
        }

        @Override
        public boolean isWrapperFor(Class<?> iface) throws SQLException {
            return dataSource.isWrapperFor(iface);
        }


    }
}
