package com.progressoft.jip9.finance;

import java.io.Serializable;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

// Aspect Oriented Programming
public class DynamicProxySample {

    public static void main(String[] args) {

        Calculator wrapped = new SimpleCalculator();

        Object proxy = Proxy.newProxyInstance(
                Thread.currentThread().getContextClassLoader(),
                new Class[]{Calculator.class, Serializable.class},
                new InvocationHandler() {
                    @Override
                    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                        return method.invoke(wrapped, args);
                    }
                });

        Calculator calculator = (Calculator) proxy;
        System.out.println(calculator.add(10, 3));
        System.out.println(calculator.subtract(3, 10));
        System.out.println("done");

    }

    private class CalculatorMonitor implements Calculator {
        private final Calculator wrapped;

        private CalculatorMonitor(Calculator wrapped) {
            this.wrapped = wrapped;
        }

        @Override
        public int add(int first, int second) {
            return wrapped.add(first, second);
        }

        @Override
        public int subtract(int first, int second) {
            return wrapped.subtract(first, second);
        }
    }
}
