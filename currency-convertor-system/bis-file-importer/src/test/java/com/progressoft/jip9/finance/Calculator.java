package com.progressoft.jip9.finance;

public interface Calculator {

    int add(int first, int second);

    int subtract(int first, int second);
}

class SimpleCalculator implements Calculator {

    @Override
    public int add(int first, int second) {
        return first + second;
    }

    @Override
    public int subtract(int first, int second) {
        return first - second;
    }
}